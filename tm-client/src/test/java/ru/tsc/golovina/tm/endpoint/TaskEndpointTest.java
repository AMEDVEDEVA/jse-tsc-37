package ru.tsc.golovina.tm.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.tsc.golovina.tm.endpoint.*;
import ru.tsc.golovina.tm.model.Task;

public class TaskEndpointTest {

    @NotNull
    private final SessionEndpoint sessionEndpoint;

    @NotNull
    private final TaskEndpoint taskEndpoint;

    @NotNull
    private final ProjectEndpoint projectEndpoint;

    @NotNull
    private final String taskName = "taskName";

    @Nullable
    private String taskId;

    @NotNull
    private Task task;

    @Nullable
    private String projectId;

    @NotNull
    private Session session;

    public TaskEndpointTest() {
        @NotNull final SessionEndpointService sessionEndpointService = new SessionEndpointService();
        sessionEndpoint = sessionEndpointService.getSessionEndpointPort();
        @NotNull final TaskEndpointService taskEndpointService = new TaskEndpointService();
        taskEndpoint = taskEndpointService.getTaskEndpointPort();
        @NotNull final ProjectEndpointService projectEndpointService = new ProjectEndpointService();
        projectEndpoint = projectEndpointService.getProjectEndpointPort();
    }

    @Before
    @SneakyThrows
    public void initializeTest() {
        session = sessionEndpoint.openSession("user", "user");
        task = taskEndpoint.createTask(session, taskName, "taskDescription");
        taskId = task.getId();
        projectId = projectEndpoint.createProject(session, "project", "project").getId();
    }

    @Test
    @SneakyThrows
    @Category(SoapCategory.class)
    public void findAllTask() {
        Assert.assertEquals(1, taskEndpoint.findTaskAll(session, Sort.CREATED.toString()).size());
    }

    @Test
    @SneakyThrows
    @Category(SoapCategory.class)
    public void findTask() {
        Assert.assertNotNull(taskId);
        Assert.assertNotNull(taskEndpoint.findTaskById(session, taskId));
        Assert.assertNotNull(taskName);
        Assert.assertNotNull(taskEndpoint.findTaskByName(session, taskName));
        Assert.assertNotNull(taskEndpoint.findTaskByIndex(session, 0));
    }

    @Test
    @SneakyThrows
    @Category(SoapCategory.class)
    public void removeByIdTask() {
        Assert.assertNotNull(taskId);
        removeTask(taskId);
        Assert.assertFalse(
                taskEndpoint
                        .findTaskAll(session)
                        .stream()
                        .anyMatch(p -> p.getId().equals(task.getId()))
        );
        taskId = null;
    }

    @Test
    @SneakyThrows
    @Category(SoapCategory.class)
    public void createTask() {
        @NotNull final String tempTaskId = taskEndpoint.createTask(session, "task", "task").getId();
        Assert.assertNotNull(tempTaskId);
        Assert.assertNotNull(taskEndpoint.findTaskById(session, tempTaskId));
        removeTask(tempTaskId);
    }

    @Test
    @SneakyThrows
    @Category(SoapCategory.class)
    public void updateByIdTask() {
        @NotNull final String newTaskName = "newTaskName";
        @NotNull final String newTaskDescription = "newTaskName";
        taskEndpoint.updateTaskById(session, taskId, newTaskName, newTaskDescription);
        @NotNull final Task updatedTask = taskEndpoint.findTaskById(session, taskId);
        Assert.assertEquals(task.getId(), updatedTask.getId());
        Assert.assertNotEquals(task.getName(), updatedTask.getName());
        Assert.assertNotEquals(task.getDescription(), updatedTask.getDescription());
        Assert.assertEquals(newTaskName, updatedTask.getName());
        Assert.assertEquals(newTaskDescription, updatedTask.getDescription());
    }

    @Test
    @SneakyThrows
    @Category(SoapCategory.class)
    public void startByIdTask() {
        Assert.assertEquals(task.getStatus(), Status.NOT_STARTED);
        taskEndpoint.startTaskById(session, taskId);
        Assert.assertEquals(taskEndpoint.findTaskById(session, taskId).getStatus(), Status.IN_PROGRESS);
    }

    @Test
    @SneakyThrows
    @Category(SoapCategory.class)
    public void finishByIdTask() {
        Assert.assertEquals(task.getStatus(), Status.NOT_STARTED);
        taskEndpoint.finishTaskById(session, taskId);
        Assert.assertEquals(taskEndpoint.findTaskById(session, taskId).getStatus(), Status.COMPLETED);
    }

    @Test
    @SneakyThrows
    @Category(SoapCategory.class)
    public void updateStatusByIdTask() {
        Assert.assertEquals(task.getStatus(), Status.NOT_STARTED);
        taskEndpoint.changeTaskStatusById(session, taskId, Status.IN_PROGRESS);
        Assert.assertEquals(taskEndpoint.findTaskById(session, taskId).getStatus(), Status.IN_PROGRESS);
        taskEndpoint.changeTaskStatusById(session, taskId, Status.COMPLETED);
        Assert.assertEquals(taskEndpoint.findTaskById(session, taskId).getStatus(), Status.COMPLETED);
    }

    @Test
    @SneakyThrows
    @Category(SoapCategory.class)
    public void bindTaskToProject() {
        Assert.assertNull(task.getProjectId());
        taskEndpoint.bindTaskById(session, projectId, taskId);
        task = taskEndpoint.findTaskById(session, taskId);
        Assert.assertNotNull(task.getProjectId());
        Assert.assertEquals(task.getProjectId(), projectId);
    }

    @Test
    @SneakyThrows
    @Category(SoapCategory.class)
    public void unbindTaskFromProject() {
        Assert.assertNull(task.getProjectId());
        taskEndpoint.bindTaskById(session, projectId, taskId);
        task = taskEndpoint.findTaskById(session, taskId);
        Assert.assertNotNull(task.getProjectId());
        Assert.assertEquals(task.getProjectId(), projectId);
        taskEndpoint.unbindTaskById(session, projectId, taskId);
        Assert.assertNull(taskEndpoint.findTaskById(session, taskId).getProjectId());
    }

    @Test
    @SneakyThrows
    @Category(SoapCategory.class)
    public void findAllTasksByProjectId() {
        Assert.assertNull(task.getProjectId());
        taskEndpoint.bindTaskById(session, projectId, taskId);
        @NotNull final String task2Id = taskEndpoint.createTask(session, "task2", "task2").getId();
        taskEndpoint.bindTaskById(session, projectId, task2Id);
        Assert.assertEquals(2, taskEndpoint.findTaskByProjectId(session, projectId).size());
        removeTask(task2Id);
    }

    @Test
    @SneakyThrows
    @Category(SoapCategory.class)
    public void removeProjectById() {
        Assert.assertNotNull(projectId);
        Assert.assertNotNull(taskId);
        taskEndpoint.bindTaskById(session, projectId, taskId);
        removeProject(projectId);
        projectId = null;
        Assert.assertTrue(projectEndpoint.findProjectAll(session).isEmpty());
        Assert.assertNull(taskEndpoint.findTaskById(session, taskId).getProjectId());
    }

    @Test
    @SneakyThrows
    @Category(SoapCategory.class)
    public void removeAllProject() {
        @NotNull final String tempTaskId = taskEndpoint.createTask(session, "t2", "t2").getId();
        @NotNull final String project2Id = projectEndpoint.createProject(session, "p2", "p2").getId();
        taskEndpoint.bindTaskById(session, projectId, taskId);
        taskEndpoint.bindTaskById(session, project2Id, tempTaskId);
        Assert.assertEquals(projectId, taskEndpoint.findTaskById(session, taskId).getProjectId());
        Assert.assertEquals(project2Id, taskEndpoint.findTaskById(session, tempTaskId).getProjectId());
        Assert.assertNotNull(projectId);
        removeProject(projectId);
        projectId = null;
        removeProject(project2Id);
        Assert.assertNull(taskEndpoint.findTaskById(session, taskId).getProjectId());
        Assert.assertNull(taskEndpoint.findTaskById(session, tempTaskId).getProjectId());
        removeTask(tempTaskId);
    }


    @After
    @SneakyThrows
    public void finalizeTest() {
        if (taskId != null) removeTask(taskId);
        if (projectId != null) removeProject(projectId);
        sessionEndpoint.closeSession(session);
    }

    @SneakyThrows
    private void removeTask(@NotNull final String id) {
        taskEndpoint.removeTaskById(session, id);
    }

    @SneakyThrows
    private void removeProject(@NotNull final String id) {
        projectEndpoint.removeProjectById(session, id);
    }

}
