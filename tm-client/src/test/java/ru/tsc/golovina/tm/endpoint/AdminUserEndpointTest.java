package ru.tsc.golovina.tm.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.tsc.golovina.tm.endpoint.*;

public class AdminUserEndpointTest {

    @NotNull
    private final AdminUserEndpoint adminUserEndpoint;

    @NotNull
    private final SessionEndpoint sessionEndpoint;

    @NotNull
    private final String userLogin = "temp";

    @NotNull
    private Session session;

    @Nullable
    private String userId;

    @NotNull
    private User user;

    @SneakyThrows
    public AdminUserEndpointTest() {
        @NotNull final AdminUserEndpointService adminUserEndpointService = new AdminUserEndpointService();
        adminUserEndpoint = adminUserEndpointService.getAdminUserEndpointPort();
        @NotNull final SessionEndpointService sessionEndpointService = new SessionEndpointService();
        sessionEndpoint = sessionEndpointService.getSessionEndpointPort();
    }

    @Before
    @SneakyThrows
    public void initializeTest() {
        session = sessionEndpoint.openSession("admin", "admin");
        user = adminUserEndpoint.createUser(session, userLogin, "temp", "a@d.com");
        userId = user.getId();
    }

    @Test
    @SneakyThrows
    @Category(SoapCategory.class)
    public void createUser() {
        @NotNull final String tempUserLogin = "daemon";
        Assert.assertNotNull(adminUserEndpoint.createUser(session, tempUserLogin, "daemon", "d@d.com"));
        Assert.assertNotNull(adminUserEndpoint.findByLoginUser(session, tempUserLogin));
        adminUserEndpoint.removeByLoginUser(session, tempUserLogin);
    }

    @Test
    @SneakyThrows
    @Category(SoapCategory.class)
    public void findAllUser() {
        Assert.assertFalse(adminUserEndpoint.findAllUser(session).isEmpty());
    }

    @Test
    @SneakyThrows
    @Category(SoapCategory.class)
    public void findByIdUser() {
        @NotNull final User tempUser = adminUserEndpoint.findByIdUser(session, userId);
        Assert.assertNotNull(tempUser);
        Assert.assertEquals(userId, tempUser.getId());
    }

    @Test
    @SneakyThrows
    @Category(SoapCategory.class)
    public void findByLoginUser() {
        Assert.assertNotNull(adminUserEndpoint.findByLoginUser(session, userLogin));
    }

    @Test
    @SneakyThrows
    @Category(SoapCategory.class)
    public void findByIndexUser() {
        Assert.assertNotNull(adminUserEndpoint.findByIndexUser(session, 1));
    }

    @Test
    @SneakyThrows
    @Category(SoapCategory.class)
    public void removeByLoginUser() {
        Assert.assertTrue(existProject());
        adminUserEndpoint.removeByLoginUser(session, userLogin);
        userId = null;
        Assert.assertFalse(existProject());
    }

    @Test
    @SneakyThrows
    @Category(SoapCategory.class)
    public void removeByIdUser() {
        Assert.assertTrue(existProject());
        adminUserEndpoint.removeByIdUser(session, userId);
        userId = null;
        Assert.assertFalse(existProject());
    }

    @Test
    @SneakyThrows
    @Category(SoapCategory.class)
    public void setRoleUser() {
        Assert.assertEquals(user.getRole(), Role.USER);
        adminUserEndpoint.setRoleUser(session, userId, Role.ADMIN);
        Assert.assertEquals(adminUserEndpoint.findByIdUser(session, userId).getRole(), Role.ADMIN);
    }

    @Test
    @SneakyThrows
    @Category(SoapCategory.class)
    public void unlockLockByLoginUser() {
        Assert.assertFalse(user.isLocked());
        adminUserEndpoint.lockByLoginUser(session, userLogin);
        Assert.assertTrue(adminUserEndpoint.findByIdUser(session, userId).isLocked());
        adminUserEndpoint.unlockByLoginUser(session, userLogin);
        Assert.assertFalse(adminUserEndpoint.findByIdUser(session, userId).isLocked());
    }

    @After
    @SneakyThrows
    public void finalizeTest() {
        if (userId != null) adminUserEndpoint.removeByIdUser(session, userId);
        sessionEndpoint.closeSession(session);
    }

    @SneakyThrows
    private boolean existProject() {
        return adminUserEndpoint
                .findAllUser(session)
                .stream()
                .anyMatch(u -> u.getId().equals(userId));
    }

}
