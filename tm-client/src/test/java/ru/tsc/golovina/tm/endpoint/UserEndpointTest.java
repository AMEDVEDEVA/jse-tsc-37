package ru.tsc.golovina.tm.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.tsc.golovina.tm.endpoint.*;

public class UserEndpointTest {

    @NotNull
    private final SessionEndpoint sessionEndpoint;

    @NotNull
    private final UserEndpoint userEndpoint;

    @NotNull
    final private String userLogin = "guest";

    @NotNull
    final private String userEmail = "g@m.ru";

    @NotNull
    private Session session;

    public UserEndpointTest() {
        @NotNull final SessionEndpointService sessionEndpointService = new SessionEndpointService();
        sessionEndpoint = sessionEndpointService.getSessionEndpointPort();
        @NotNull final UserEndpointService userEndpointService = new UserEndpointService();
        userEndpoint = userEndpointService.getUserEndpointPort();
    }

    @Before
    @SneakyThrows
    public void initializeTest() {
        session = sessionEndpoint.register(userLogin, "guest", userEmail);
    }

    @Test
    @SneakyThrows
    @Category(SoapCategory.class)
    public void setPasswordUser() {
        @NotNull final String oldPassword = userEndpoint.getCurrentUser(session).getPasswordHash();
        userEndpoint.updateUserPassword(session, "newPassword");
        @NotNull final String newPassword = userEndpoint.getCurrentUser(session).getPasswordHash();
        Assert.assertNotEquals(oldPassword, newPassword);
    }

    @Test
    @SneakyThrows
    @Category(SoapCategory.class)
    public void updateByIdUser() {
        @NotNull final User oldUser = userEndpoint.getCurrentUser(session);
        Assert.assertNull(oldUser.getLastName());
        Assert.assertNull(oldUser.getFirstName());
        Assert.assertNull(oldUser.getMiddleName());
        Assert.assertEquals(userEmail, oldUser.getEmail());
        @NotNull final String lastName = "lastName",
                firstName = "firstName",
                middleName = "middleName",
                newEmail = "newEmail";
        userEndpoint.updateUserById(session, session.getUserId(), lastName, firstName, middleName, newEmail);
        @NotNull final User updatedUser = userEndpoint.getCurrentUser(session);
        Assert.assertEquals(lastName, updatedUser.getLastName());
        Assert.assertEquals(firstName, updatedUser.getFirstName());
        Assert.assertEquals(middleName, updatedUser.getMiddleName());
        Assert.assertEquals(newEmail, updatedUser.getEmail());
    }

    @Test
    @SneakyThrows
    @Category(SoapCategory.class)
    public void getUser() {
        @NotNull final User user = userEndpoint.getCurrentUser(session);
        Assert.assertNotNull(user);
        Assert.assertEquals(userEmail, user.getEmail());
        Assert.assertEquals(userLogin, user.getLogin());
    }

    @After
    @SneakyThrows
    public void finalizeTest() {
        @NotNull final AdminUserEndpointService adminUserEndpointService = new AdminUserEndpointService();
        @NotNull final AdminUserEndpoint adminUserEndpoint = adminUserEndpointService.getAdminUserEndpointPort();
        Assert.assertTrue(sessionEndpoint.closeSession(session));
        @NotNull final Session tempSession = sessionEndpoint.openSession("admin", "admin");
        adminUserEndpoint.removeByLoginUser(tempSession, userLogin);
        Assert.assertTrue(sessionEndpoint.closeSession(tempSession));
    }

}
