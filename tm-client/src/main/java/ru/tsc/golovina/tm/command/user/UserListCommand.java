package ru.tsc.golovina.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.golovina.tm.command.AbstractUserCommand;
import ru.tsc.golovina.tm.endpoint.Role;
import ru.tsc.golovina.tm.endpoint.Session;
import ru.tsc.golovina.tm.endpoint.User;
import ru.tsc.golovina.tm.exception.empty.EmptyUserListException;

import java.util.List;

public final class UserListCommand extends AbstractUserCommand {

    @Nullable
    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

    @NotNull
    @Override
    public String getCommand() {
        return "user-list";
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Display list of users";
    }

    @Override
    public void execute() {
        @NotNull final Session session = serviceLocator.getSession();
        System.out.println("User list");
        @Nullable final List<User> users = serviceLocator.getUserEndpoint().findAllUser(session);
        if (null == users) throw new EmptyUserListException();
        for (final User user : users)
            showUser(user);
    }

}
