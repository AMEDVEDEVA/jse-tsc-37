package ru.tsc.golovina.tm;

import org.jetbrains.annotations.NotNull;
import ru.tsc.golovina.tm.component.Bootstrap;
import ru.tsc.golovina.tm.util.SystemUtil;

public class Application {

    public static void main(String[] args) {
        System.out.println("PID: " + SystemUtil.getPID());
        @NotNull final Bootstrap bootstrap = new Bootstrap();
        bootstrap.start(args);
    }

}