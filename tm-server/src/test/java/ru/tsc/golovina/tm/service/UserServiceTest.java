package ru.tsc.golovina.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Test;
import ru.tsc.golovina.tm.api.IPropertyService;
import ru.tsc.golovina.tm.api.repository.IUserRepository;
import ru.tsc.golovina.tm.api.service.IUserService;
import ru.tsc.golovina.tm.enumerated.Role;
import ru.tsc.golovina.tm.exception.AbstractException;
import ru.tsc.golovina.tm.model.User;
import ru.tsc.golovina.tm.repository.UserRepository;
import ru.tsc.golovina.tm.util.HashUtil;

public class UserServiceTest {

    @NotNull
    private final IUserService userService;

    @NotNull
    private final IPropertyService propertyService;

    @NotNull
    private final User user;

    @NotNull
    private final String userId;

    @NotNull
    final private String userLogin = "login";

    @Nullable
    final private String userPassword = "password";

    public UserServiceTest() throws AbstractException {
        user = new User();
        userId = user.getId();
        user.setLogin(userLogin);
        user.setPassword(HashUtil.salt( "qwe", 3,userPassword));
        @NotNull final IUserRepository userRepository = new UserRepository();
        userRepository.add(user);
        propertyService = new PropertyService();
        userService = new UserService(userRepository, propertyService);
    }

    @Test
    public void create() throws AbstractException {
        Assert.assertEquals(1, userService.findAll().size());
        @NotNull final String newUserLogin = "newUserLogin";
        @NotNull final String newUserPassword = "newUserPassword";
        @NotNull final User newUser = userService.create(newUserLogin, newUserPassword);
        Assert.assertEquals(newUserLogin, newUser.getLogin());
        int iteration = propertyService.getPasswordIteration();
        @NotNull final String secret = propertyService.getPasswordSecret();
        Assert.assertEquals(HashUtil.salt(secret, iteration, newUserPassword), newUser.getPassword());
    }

    @Test
    public void findByLogin() throws AbstractException {
        @NotNull final User tempUser = userService.findUserByLogin(userLogin);
        Assert.assertEquals(user, tempUser);
        Assert.assertEquals(userLogin, tempUser.getLogin());
        Assert.assertEquals(HashUtil.salt( "qwe", 3,userPassword), tempUser.getPassword());
    }

    @Test
    public void removeByLogin() throws AbstractException {
        Assert.assertFalse(userService.findAll().isEmpty());
        Assert.assertEquals(1, userService.findAll().size());
        userService.removeUserByLogin(userLogin);
        Assert.assertTrue(userService.findAll().isEmpty());
    }

    @Test
    public void setPassword() throws AbstractException {
        Assert.assertEquals(HashUtil.salt( "qwe", 3,userPassword), user.getPassword());
        @NotNull final String newPassword = "newPassword";
        userService.setPassword(userId, newPassword);
        Assert.assertNotEquals(HashUtil.salt( "qwe", 3,userPassword), user.getPassword());
        int iteration = propertyService.getPasswordIteration();
        @NotNull final String secret = propertyService.getPasswordSecret();
        Assert.assertEquals(HashUtil.salt(secret, iteration, newPassword), user.getPassword());
    }

    @Test
    public void setRole() throws AbstractException {
        Assert.assertNotNull(user.getRole());
        Assert.assertEquals(user.getRole(), Role.USER);
        userService.setRole(userId, Role.ADMIN);
        Assert.assertEquals(user.getRole(), Role.ADMIN);
    }

    @Test
    public void updateById() throws AbstractException {
        @NotNull final String newLastName = "newLastName";
        @NotNull final String newFirstName = "newFirstName";
        @NotNull final String newMiddleName = "newMiddleName";
        @NotNull final String newEmail = "email";
        Assert.assertNull(user.getLastName());
        Assert.assertNull(user.getFirstName());
        Assert.assertNull(user.getMiddleName());
        Assert.assertNull(user.getEmail());
        userService.updateUserById(userId, newLastName, newFirstName, newMiddleName, newEmail);
        Assert.assertEquals(newLastName, user.getLastName());
        Assert.assertEquals(newFirstName, user.getFirstName());
        Assert.assertEquals(newMiddleName, user.getMiddleName());
        Assert.assertEquals(newEmail, user.getEmail());
    }

    @Test
    public void lockUnlockByLogin() throws AbstractException {
        Assert.assertFalse(user.getLocked());
        userService.lockUserByLogin(userLogin);
        Assert.assertTrue(user.getLocked());
        userService.unlockUserByLogin(userLogin);
        Assert.assertFalse(user.getLocked());
    }

}
