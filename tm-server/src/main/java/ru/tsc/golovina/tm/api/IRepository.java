package ru.tsc.golovina.tm.api;

import org.apache.ibatis.annotations.Param;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.golovina.tm.model.AbstractEntity;

import java.sql.SQLException;
import java.util.Comparator;
import java.util.List;

public interface IRepository<E extends AbstractEntity> {

    void add(@NotNull E entity) throws SQLException;

    void addAll(@NotNull List<E> entities);

    void remove(@NotNull E entity) throws SQLException;

    @NotNull
    List<E> findAll();

    @NotNull
    List<E> findAll(@NotNull Comparator<E> comparator);

    boolean existsById(@NotNull @Param("id") String id) throws SQLException;

    boolean existsByIndex(@NotNull Integer index) throws SQLException;

    void clear() throws SQLException;

    @NotNull
    E findById(@NotNull String id) throws SQLException;

    @Nullable
    E findByIndex(@NotNull Integer index) throws SQLException;

    @Nullable
    E removeById(@NotNull String id) throws SQLException;

    @Nullable
    E removeByIndex(@NotNull Integer index) throws SQLException;

    @NotNull
    Integer getSize() throws SQLException;

}
