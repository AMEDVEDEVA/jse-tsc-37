package ru.tsc.golovina.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.golovina.tm.api.repository.IProjectRepository;
import ru.tsc.golovina.tm.api.repository.ITaskRepository;
import ru.tsc.golovina.tm.api.service.IConnectionService;
import ru.tsc.golovina.tm.api.service.IProjectTaskService;
import ru.tsc.golovina.tm.exception.empty.EmptyIdException;
import ru.tsc.golovina.tm.exception.empty.EmptyNameException;
import ru.tsc.golovina.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.golovina.tm.exception.system.DatabaseException;
import ru.tsc.golovina.tm.model.Project;
import ru.tsc.golovina.tm.model.Task;
import ru.tsc.golovina.tm.repository.ProjectRepository;
import ru.tsc.golovina.tm.repository.TaskRepository;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

public final class ProjectTaskService implements IProjectTaskService {

    @NotNull
    private final IConnectionService connectionService;

    public ProjectTaskService(@NotNull IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

    @NotNull
    @Override
    public List<Task> findTaskByProjectId(@NotNull final String userId, @Nullable final String projectId) {
        if (projectId == null || projectId.isEmpty()) throw new EmptyIdException();
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final IProjectRepository projectRepository = new ProjectRepository(connection);
            @NotNull final ITaskRepository taskRepository = new TaskRepository(connection);
            if (!projectRepository.existsById(projectId)) throw new ProjectNotFoundException();
            return taskRepository.findAllTaskByProjectId(userId, projectId);
        } catch (SQLException exception) {
            throw new DatabaseException();
        }
    }

    @Override
    public void bindTaskById(@NotNull final String userId, @Nullable final String projectId,
                             @Nullable final String taskId) {
        if (projectId == null || projectId.isEmpty()) return;
        if (taskId == null || taskId.isEmpty()) return;
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final IProjectRepository projectRepository = new ProjectRepository(connection);
            @NotNull final ITaskRepository taskRepository = new TaskRepository(connection);
            if (!projectRepository.existsById(projectId) || !taskRepository.existsById(taskId)) return;
            taskRepository.bindTaskToProjectById(userId, projectId, taskId);
            connection.commit();
        } catch (SQLException exception) {
            throw new DatabaseException();
        }
    }

    @Override
    public void unbindTaskById(@NotNull final String userId, @Nullable final String projectId,
                               @Nullable final String taskId) {
        if (projectId == null || projectId.isEmpty()) return;
        if (taskId == null || taskId.isEmpty()) return;
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final IProjectRepository projectRepository = new ProjectRepository(connection);
            @NotNull final ITaskRepository taskRepository = new TaskRepository(connection);
            if (!projectRepository.existsById(projectId) || !taskRepository.existsById(taskId)) return;
            taskRepository.unbindTaskById(userId, taskId);
            connection.commit();
        } catch (SQLException exception) {
            throw new DatabaseException();
        }
    }

    @Override
    public void removeById(@NotNull final String userId, @Nullable final String projectId) {
        if (projectId == null) throw new EmptyIdException();
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final IProjectRepository projectRepository = new ProjectRepository(connection);
            @NotNull final ITaskRepository taskRepository = new TaskRepository(connection);
            taskRepository.removeAllTaskByProjectId(userId, projectId);
            projectRepository.removeById(projectId);
            connection.commit();
        } catch (SQLException exception) {
            throw new DatabaseException();
        }
    }

    @Override
    public void removeByIndex(@NotNull final String userId, @NotNull final Integer index) {
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final IProjectRepository projectRepository = new ProjectRepository(connection);
            @NotNull final ITaskRepository taskRepository = new TaskRepository(connection);
            @Nullable final Project project = projectRepository.findByIndex(index);
            if (project == null) throw new ProjectNotFoundException();
            @NotNull final String projectId = project.getId();
            taskRepository.removeAllTaskByProjectId(userId, projectId);
            projectRepository.removeById(projectId);
            connection.commit();
        } catch (SQLException exception) {
            throw new DatabaseException();
        }
    }

    @Override
    public void removeByName(@NotNull final String userId, @Nullable final String name) {
        if (name == null) throw new EmptyNameException();
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final IProjectRepository projectRepository = new ProjectRepository(connection);
            @NotNull final ITaskRepository taskRepository = new TaskRepository(connection);
            @NotNull final Project project = projectRepository.findByName(userId, name);
            @NotNull final String projectId = project.getId();
            taskRepository.removeAllTaskByProjectId(userId, projectId);
            projectRepository.removeById(projectId);
            connection.commit();
        } catch (SQLException exception) {
            throw new DatabaseException();
        }
    }

}
