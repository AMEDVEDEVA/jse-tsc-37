package ru.tsc.golovina.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.golovina.tm.api.IService;
import ru.tsc.golovina.tm.enumerated.Status;
import ru.tsc.golovina.tm.model.AbstractOwnerEntity;

import java.util.Comparator;
import java.util.List;

public interface IOwnerService<E extends AbstractOwnerEntity> extends IService<E> {

    boolean existById(@NotNull String userId, @NotNull String id);

    E findByIndex(@Nullable String userId, @Nullable Integer index);

    void clear(@Nullable String userId);

    void remove(@NotNull String userId, @NotNull E entity);

    @NotNull
    List<E> findAll(@NotNull String userId, @Nullable final String sort);

    @NotNull List<E> findAll(@Nullable String userId);

    @NotNull
    List<E> findAll(@NotNull String userId, @NotNull Comparator<E> comparator);

    void add(@Nullable String userId, E entity);

    void addAll(@Nullable String userId, @NotNull List<E> entities);

    boolean existsById(@Nullable String userId, @NotNull String id);

    boolean existsByIndex(@Nullable String userId, @NotNull Integer index);

    @NotNull
    E findById(@NotNull String userId, @NotNull String id);

    @NotNull
    E findByIndex(@NotNull String userId, int index);

    @NotNull Integer getSize(@NotNull String userId);

    E create(@Nullable String userId, @Nullable String name);

    E findByName(@Nullable String userId, @Nullable String name);

    E removeByName(@Nullable String userId, @Nullable String name);

    E removeById(@NotNull String userId, @NotNull String id);

    void removeByIndex(@NotNull String userId, int index);

    E removeByIndex(@Nullable String userId, @NotNull Integer index);

    void updateById(@Nullable String userId, @Nullable String id, @Nullable String name,
                    @NotNull String description);

    void updateByIndex(@Nullable String userId, @Nullable Integer index, @Nullable String name,
                       @NotNull String description);

    boolean existsByName(@Nullable String userId, @NotNull String name);

    E startById(@Nullable String userId, @Nullable String id);

    E startByIndex(@Nullable String userId, @Nullable Integer index);

    E startByName(@Nullable String userId, @Nullable String name);

    E finishById(@Nullable String userId, @Nullable String id);

    E finishByIndex(@Nullable String userId, @Nullable Integer index);

    E finishByName(@Nullable String userId, @Nullable String name);

    E changeStatusById(@Nullable String userId, @Nullable String id, @Nullable Status status);

    E changeStatusByIndex(@Nullable String userId, @Nullable Integer index,
                          @Nullable Status status);

    E changeStatusByName(@Nullable String userId, @Nullable String name,
                         @Nullable Status status);
}
