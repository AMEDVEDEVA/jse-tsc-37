package ru.tsc.golovina.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.tsc.golovina.tm.api.repository.ISessionRepository;
import ru.tsc.golovina.tm.exception.AbstractException;
import ru.tsc.golovina.tm.model.Session;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class SessionRepository extends AbstractRepository<Session> implements ISessionRepository {

    public SessionRepository(@NotNull Connection connection) {
        super(connection);
    }

    @Override
    protected @NotNull Session fetch(@NotNull ResultSet row) throws AbstractException, SQLException {
        if (row == null) return null;
        @NotNull final Session session = new Session();
        session.setUserId(row.getString("user_id"));
        session.setTimestamp(row.getLong("timestamp"));
        session.setId(row.getString("id"));
        session.setSignature(row.getString("signature"));
        return session;
    }

    protected String getTableName() {
        return "session";
    }

    @Override
    public Session contains(@NotNull final String id) throws SQLException {
        @NotNull final String query =
                "SELECT * FROM " + getTableName() + " " +
                        "WHERE id = ? LIMIT 1";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, id);
        final boolean result = statement.executeQuery().first();
        statement.close();
        return result;
    }

}
