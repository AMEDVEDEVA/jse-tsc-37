package ru.tsc.golovina.tm.exception.empty;

import ru.tsc.golovina.tm.exception.AbstractException;

public class EmptyLoginException extends AbstractException {

    public EmptyLoginException() {
        super("Error. Login is empty.");
    }

}
