package ru.tsc.golovina.tm.api.service;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.jetbrains.annotations.NotNull;

import java.sql.Connection;

public interface IConnectionService {

    @NotNull
    Connection getConnection();

    @NotNull
    SqlSessionFactory getSqlSessionFactory();

    @NotNull
    SqlSession getSqlSession();

}
