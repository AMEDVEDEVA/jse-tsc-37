package ru.tsc.golovina.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.tsc.golovina.tm.api.IPropertyService;

public interface IServiceLocator {

    @NotNull
    ITaskService getTaskService();

    @NotNull
    IProjectService getProjectService();

    @NotNull
    IProjectTaskService getProjectTaskService();

    @NotNull
    IUserService getUserService();

    @NotNull
    IPropertyService getPropertyService();

}
