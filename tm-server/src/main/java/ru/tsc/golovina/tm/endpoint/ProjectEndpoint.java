package ru.tsc.golovina.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.golovina.tm.api.endpoint.IProjectEndpoint;
import ru.tsc.golovina.tm.api.service.IProjectService;
import ru.tsc.golovina.tm.api.service.ISessionService;
import ru.tsc.golovina.tm.enumerated.Status;
import ru.tsc.golovina.tm.model.Project;
import ru.tsc.golovina.tm.model.Session;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public class ProjectEndpoint implements IProjectEndpoint {

    private IProjectService projectService;

    private ISessionService sessionService;

    public ProjectEndpoint() {

    }

    public ProjectEndpoint(final IProjectService projectService, ISessionService sessionService) {
        this.projectService = projectService;
        this.sessionService = sessionService;
    }

    @Override
    @WebMethod
    public @NotNull List<Project> findAllByUserId(@Nullable @WebParam(name = "session") Session session) {
        sessionService.validate(session);
        return projectService.findAll(session.getUserId());
    }

    @Override
    @WebMethod
    public @NotNull Project findByIdUserId(
            @Nullable @WebParam(name = "session") Session session,
            @Nullable @WebParam(name = "id") String id
    ) {
        sessionService.validate(session);
        return projectService.findById(session.getUserId(), id);
    }

    @Override
    @WebMethod
    public @NotNull Project findByIndexUserId(
            @Nullable @WebParam(name = "session") Session session,
            @Nullable @WebParam(name = "index") Integer index
    ) {
        sessionService.validate(session);
        return projectService.findByIndex(session.getUserId(), index);
    }

    @Override
    @WebMethod
    public void clearByUserId(@Nullable @WebParam(name = "session") Session session) {
        sessionService.validate(session);
        projectService.clear(session.getUserId());
    }

    @Override
    @WebMethod
    public void removeByUserId(
            @Nullable @WebParam(name = "session") Session session,
            @Nullable @WebParam(name = "entity") Project entity
    ) {
        sessionService.validate(session);
        projectService.remove(session.getUserId(), entity);
    }

    @Override
    @WebMethod
    public @NotNull Integer getSizeByUserId(@Nullable @WebParam(name = "session") Session session) {
        sessionService.validate(session);
        return projectService.getSize(session.getUserId());
    }

    @Override
    @WebMethod
    public void add(@Nullable @WebParam(name = "session") Session session,
                    @Nullable @WebParam(name = "entity") Project entity
    ) {
        sessionService.validate(session);
        projectService.add(session.getUserId(), entity);
    }

    @Override
    @WebMethod
    public void addAll(
            @Nullable @WebParam(name = "session") Session session,
            @Nullable @WebParam(name = "entities") List<Project> entities
    ) {
        sessionService.validate(session);
        projectService.addAll(session.getUserId(), entities);
    }

    @Override
    @WebMethod
    public void remove(
            @Nullable @WebParam(name = "session") Session session,
            @Nullable @WebParam(name = "entity") Project entity
    ) {
        sessionService.validate(session);
        projectService.remove(session.getUserId(), entity);
    }

    @Override
    @WebMethod
    public @NotNull List<Project> findAll(@Nullable @WebParam(name = "session") Session session) {
        sessionService.validate(session);
        return projectService.findAll(session.getUserId());
    }

    @Override
    @WebMethod
    public void clear(@Nullable @WebParam(name = "session") Session session) {
        sessionService.validate(session);
        projectService.clear(session.getUserId());
    }

    @Override
    @WebMethod
    public @NotNull Project findById(
            @Nullable @WebParam(name = "session") Session session,
            @Nullable @WebParam(name = "id") String id
    ) {
        sessionService.validate(session);
        return projectService.findById(session.getUserId(), id);
    }

    @Override
    @WebMethod
    public @NotNull Project findByIndex(
            @Nullable @WebParam(name = "session") Session session,
            @NotNull @WebParam(name = "index") Integer index
    ) {
        sessionService.validate(session);
        return projectService.findByIndex(session.getUserId(), index);
    }

    @Override
    @WebMethod
    public boolean existsById(
            @Nullable @WebParam(name = "session") Session session,
            @Nullable @WebParam(name = "id") String id
    ) {
        sessionService.validate(session);
        return projectService.existsById(session.getUserId(), id);
    }

    @Override
    @WebMethod
    public boolean existsByIndex(
            @Nullable @WebParam(name = "session") Session session,
            @Nullable @WebParam(name = "index") Integer index
    ) {
        sessionService.validate(session);
        return projectService.existsByIndex(session.getUserId(), index);
    }

    @Override
    @WebMethod
    public @NotNull Project removeById(
            @Nullable @WebParam(name = "session") Session session,
            @NotNull @WebParam(name = "id") String id
    ) {
        sessionService.validate(session);
        return projectService.removeById(session.getUserId(), id);
    }

    @Override
    @WebMethod
    public @NotNull Project removeByIndex(
            @Nullable @WebParam(name = "session") Session session,
            @Nullable @WebParam(name = "index") Integer index
    ) {
        sessionService.validate(session);
        return projectService.removeByIndex(session.getUserId(), index);
    }

    @Override
    @WebMethod
    public @NotNull Integer getSize(@Nullable @WebParam(name = "session") Session session) {
        sessionService.validate(session);
        return projectService.getSize();
    }

    @Override
    @WebMethod
    public void create(
            @Nullable @WebParam(name = "session") Session session,
            @Nullable @WebParam(name = "name") String name
    ) {
        sessionService.validate(session);
        projectService.create(session.getUserId(), name);
    }

    @Override
    @WebMethod
    public void createWithDescription(
            @Nullable @WebParam(name = "session") Session session,
            @Nullable @WebParam(name = "name") String name,
            @Nullable @WebParam(name = "description") String description
    ) {
        sessionService.validate(session);
        projectService.create(session.getUserId(), name, description);
    }

    @Override
    @WebMethod
    public @NotNull Project findByName(
            @Nullable @WebParam(name = "session") Session session,
            @Nullable @WebParam(name = "name") String name
    ) {
        sessionService.validate(session);
        return projectService.findByName(session.getUserId(), name);
    }

    @Override
    @WebMethod
    public void updateById(
            @Nullable @WebParam(name = "session") Session session,
            @Nullable @WebParam(name = "id") String id,
            @Nullable @WebParam(name = "name") String name,
            @NotNull @WebParam(name = "description") String description
    ) {
        sessionService.validate(session);
        projectService.updateById(session.getUserId(), id, name, description);
    }

    @Override
    @WebMethod
    public void updateByIndex(
            @Nullable @WebParam(name = "session") Session session,
            @Nullable @WebParam(name = "index") Integer index,
            @Nullable @WebParam(name = "name") String name,
            @NotNull @WebParam(name = "description") String description
    ) {
        sessionService.validate(session);
        projectService.updateByIndex(session.getUserId(), index, name, description);
    }

    @Override
    @WebMethod
    public boolean existsByName(
            @Nullable @WebParam(name = "session") Session session,
            @NotNull @WebParam(name = "name") String name
    ) {
        sessionService.validate(session);
        return projectService.existsByName(session.getUserId(), name);
    }

    @Override
    @WebMethod
    public @NotNull Project startById(
            @Nullable @WebParam(name = "session") Session session,
            @Nullable @WebParam(name = "id") String id
    ) {
        sessionService.validate(session);
        return projectService.startById(session.getUserId(), id);
    }

    @Override
    @WebMethod
    public @NotNull Project startByIndex(
            @Nullable @WebParam(name = "session") Session session,
            @Nullable @WebParam(name = "index") Integer index
    ) {
        sessionService.validate(session);
        return projectService.startByIndex(session.getUserId(), index);
    }

    @Override
    @WebMethod
    public @NotNull Project startByName(
            @Nullable @WebParam(name = "session") Session session,
            @Nullable @WebParam(name = "name") String name
    ) {
        sessionService.validate(session);
        return projectService.startByName(session.getUserId(), name);
    }

    @Override
    @WebMethod
    public @NotNull Project finishById(
            @Nullable @WebParam(name = "session") Session session,
            @Nullable @WebParam(name = "id") String id
    ) {
        sessionService.validate(session);
        return projectService.finishById(session.getUserId(), id);
    }

    @Override
    @WebMethod
    public @NotNull Project finishByIndex(
            @Nullable @WebParam(name = "session") Session session,
            @Nullable @WebParam(name = "index") Integer index
    ) {
        sessionService.validate(session);
        return projectService.finishByIndex(session.getUserId(), index);
    }

    @Override
    @WebMethod
    public @NotNull Project finishByName(
            @Nullable @WebParam(name = "session") Session session,
            @Nullable @WebParam(name = "name") String name
    ) {
        sessionService.validate(session);
        return projectService.finishByName(session.getUserId(), name);
    }

    @Override
    @WebMethod
    public @NotNull Project changeStatusById(
            @Nullable @WebParam(name = "session") Session session,
            @Nullable @WebParam(name = "id") String id,
            @Nullable @WebParam(name = "status") Status status
    ) {
        sessionService.validate(session);
        return projectService.changeStatusById(session.getUserId(), id, status);
    }

    @Override
    @WebMethod
    public @NotNull Project changeStatusByIndex(
            @Nullable @WebParam(name = "session") Session session,
            @Nullable @WebParam(name = "index") Integer index,
            @Nullable @WebParam(name = "status") Status status
    ) {
        sessionService.validate(session);
        return projectService.changeStatusByIndex(session.getUserId(), index, status);
    }

    @Override
    @WebMethod
    public @NotNull Project changeStatusByName(
            @Nullable @WebParam(name = "session") Session session,
            @Nullable @WebParam(name = "name") String name,
            @Nullable @WebParam(name = "status") Status status
    ) {
        sessionService.validate(session);
        return projectService.changeStatusByName(session.getUserId(), name, status);
    }

}
