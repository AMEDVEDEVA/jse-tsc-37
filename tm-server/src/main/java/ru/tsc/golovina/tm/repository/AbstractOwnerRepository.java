package ru.tsc.golovina.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.golovina.tm.api.repository.IOwnerRepository;
import ru.tsc.golovina.tm.enumerated.Status;
import ru.tsc.golovina.tm.exception.entity.EntityNotFoundException;
import ru.tsc.golovina.tm.exception.system.IndexIncorrectException;
import ru.tsc.golovina.tm.model.AbstractOwnerEntity;

import java.sql.*;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractOwnerRepository<E extends AbstractOwnerEntity> extends AbstractRepository<E>
        implements IOwnerRepository<E> {

    public AbstractOwnerRepository(@NotNull Connection connection) {
        super(connection);
    }

    @NotNull
    @Override
    public E add(@NotNull final String userId, @NotNull final E entity) throws SQLException {
        @NotNull final String query = "INSERT INTO " + getTableName() +
                " (id, name, description, status, create_date, start_date, user_id) " +
                "VALUES (?, ?, ?, ?, ?, ?, ?);";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, entity.getId());
        statement.setString(2, entity.getName());
        statement.setString(3, entity.getDescription());
        statement.setString(4, entity.getStatus().toString());
        statement.setTimestamp(5, new Timestamp(entity.getCreateDate().getTime()));
        @Nullable final Date startDate = entity.getStartDate();
        statement.setTimestamp(6, startDate == null ? null : new Timestamp(startDate.getTime()));
        statement.setString(7, entity.getUserId());
        statement.executeUpdate();
        statement.close();
    }

    @NotNull
    @Override
    public List<E> findAll(@NotNull final String userId) throws SQLException {
        @NotNull final String query = "SELECT * FROM " + getTableName() + " WHERE user_id = ?;";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, userId);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        @NotNull final List<E> result = new ArrayList<>();
        while (resultSet.next()) result.add(fetch(resultSet));
        statement.close();
        if (result.isEmpty()) throw new EntityNotFoundException();
        return result;
    }

    @NotNull
    @Override
    public List<E> findAll(@NotNull final String userId, @NotNull final Comparator<E> comparator) throws SQLException {
        List<E> entities = findAll(userId);
        entities.sort(comparator);
        return entities;
    }

    @Nullable
    @Override
    public E findById(@NotNull final String userId, @NotNull final String id) throws SQLException {
        @NotNull final String query = "SELECT * FROM " + getTableName() + " WHERE user_id = ? AND id = ?;";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, userId);
        statement.setString(2, id);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        if (!resultSet.next()) throw new EntityNotFoundException();
        @NotNull final E entity = fetch(resultSet);
        statement.close();
        return entity;
    }

    @NotNull
    @Override
    public E findByIndex(@NotNull final String userId, @NotNull final Integer index) throws SQLException {
        if (index < 0) throw new IndexIncorrectException();
        @NotNull final String query = "SELECT * FROM " + getTableName() + " WHERE user_id = ? LIMIT 1 OFFSET ?;";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, userId);
        statement.setInt(2, index);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        if (!resultSet.next()) throw new IndexIncorrectException();
        @NotNull final E entity = fetch(resultSet);
        statement.close();
        return entity;

    }

    @Override
    public void clear(@NotNull final String userId) throws SQLException {
        @NotNull final String query = "DELETE FROM " + getTableName() + " WHERE user_id = ?;";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, userId);
        statement.execute();
        statement.close();
    }

    @Override
    public void removeById(@NotNull final String userId, @NotNull final String id) throws SQLException {
        @NotNull final String query = "DELETE FROM " + getTableName() + " WHERE user_id = ? AND id = ?;";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, userId);
        statement.setString(2, id);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        statement.close();
    }

    @Nullable
    @Override
    public E removeByIndex(@NotNull final String userId, @NotNull final Integer index) throws SQLException {
        final @NotNull E entity = findByIndex(userId, index);
        remove(userId, entity);
        return entity;
    }

    @Override
    public void remove(@NotNull final String userId, @NotNull final E entity) throws SQLException {
        @NotNull final String id = entity.getId();
        @NotNull final String query = "DELETE FROM " + getTableName() + " WHERE user_id = ? AND id = ?;";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, userId);
        statement.setString(2, id);
        statement.execute();
        statement.close();
    }

    @NotNull
    public Integer getSize(@NotNull final String userId) throws SQLException {
        @NotNull final String query = "SELECT COUNT(*) FROM " + getTableName() + " WHERE user_id = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, userId);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        statement.close();
        return resultSet.getInt("total");
    }

    @Override
    public boolean existsByName(@Nullable String userId, @NotNull String name) throws SQLException {
        @NotNull final String query = "SELECT COUNT(*) FROM " + getTableName() + " WHERE user_id = ? AND id = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, userId);
        statement.setString(2, name);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        if (!resultSet.next()) return false;
        return resultSet.getInt(1) >= 1;
    }

    @Override
    public @NotNull E findByName(@NotNull final String userId, @NotNull final String name) throws SQLException {
        @NotNull final String query = "SELECT * FROM " + getTableName() +
                " WHERE user_id = ? AND name = ?;";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, userId);
        statement.setString(2, name);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        if (!resultSet.next()) throw new EntityNotFoundException();
        @NotNull final E entity = fetch(resultSet);
        statement.close();
        return entity;
    }

    @Override
    public @Nullable E startById(@NotNull final String userId, @NotNull final String id) throws SQLException {
        @NotNull final E entity = findById(userId, id);
        @NotNull final String query = "UPDATE " + getTableName() +
                " SET status = ?, start_date = ?" +
                " WHERE user_id = ? and id = ?;";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, Status.IN_PROGRESS.toString());
        statement.setTimestamp(2, new Timestamp(new Date().getTime()));
        statement.setString(3, userId);
        statement.setString(4, id);
        statement.executeUpdate();
        statement.close();
        return entity;
    }

    @Override
    public @NotNull E startByIndex(@NotNull final String userId, @NotNull final Integer index) throws SQLException {
        @NotNull final E entity = findByIndex(userId, index);
        @NotNull final String projectId = entity.getId();
        @NotNull final String query = "UPDATE " + getTableName() +
                " SET status = ?, start_date = ?" +
                " WHERE user_id = ? and id = ?;";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, Status.IN_PROGRESS.toString());
        statement.setTimestamp(2, new Timestamp(new Date().getTime()));
        statement.setString(3, userId);
        statement.setString(4, projectId);
        statement.executeUpdate();
        statement.close();
        return entity;
    }

    @Override
    public @NotNull E startByName(@NotNull final String userId, @NotNull final String name) throws SQLException {
        @NotNull final E entity = findByName(userId, name);
        @NotNull final String query = "UPDATE " + getTableName() +
                " SET status = ?, start_date = ?" +
                " WHERE user_id = ? and name = ?;";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, Status.IN_PROGRESS.toString());
        statement.setTimestamp(2, new Timestamp(new Date().getTime()));
        statement.setString(3, userId);
        statement.setString(4, name);
        statement.executeUpdate();
        statement.close();
        return entity;
    }

    @Override
    public @Nullable E finishById(@NotNull final String userId, @NotNull final String id) throws SQLException {
        @Nullable final E entity = findById(userId, id);
        if (entity == null) return null;
        @NotNull final String query = "UPDATE " + getTableName() +
                " SET status = ?" +
                " WHERE user_id = ? and id = ?;";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, Status.COMPLETED.toString());
        statement.setString(2, userId);
        statement.setString(3, id);
        statement.executeUpdate();
        statement.close();
        return entity;
    }

    @Override
    public @NotNull E finishByIndex(@NotNull final String userId, @NotNull final Integer index) throws SQLException {
        @NotNull final E entity = findByIndex(userId, index);
        @NotNull final String projectId = entity.getId();
        @NotNull final String query = "UPDATE " + getTableName() +
                " SET status = ?" +
                " WHERE user_id = ? AND id = ?;";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, Status.COMPLETED.toString());
        statement.setString(2, userId);
        statement.setString(3, projectId);
        statement.executeUpdate();
        statement.close();
        return entity;
    }

    @Override
    public @NotNull E finishByName(@NotNull final String userId, @NotNull final String name) throws SQLException {
        @NotNull final E entity = findByName(userId, name);
        @NotNull final String query = "UPDATE " + getTableName() +
                " SET status = ?" +
                " WHERE user_id = ? and name = ?;";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, Status.COMPLETED.toString());
        statement.setString(2, userId);
        statement.setString(3, name);
        statement.executeUpdate();
        statement.close();
        return entity;
    }

    @Override
    public @Nullable E changeStatusById(@NotNull final String userId,
                                        @NotNull final String id,
                                        @NotNull final Status status) throws SQLException
    {
        @Nullable final E entity = findById(userId, id);
        if (entity == null) return null;
        @NotNull final String subQueryUpdate = status.compareTo(Status.IN_PROGRESS) > 0 ?
                ", start_date = current_timestamp" : "";
        @NotNull final String query = "UPDATE " + getTableName() +
                " SET status = ? " + subQueryUpdate +
                " WHERE user_id = ? and id = ?;";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, status.toString());
        statement.setString(2, userId);
        statement.setString(3, id);
        statement.executeUpdate();
        statement.close();
        return entity;
    }

    @Override
    public @NotNull E changeStatusByIndex(@NotNull final String userId,
                                          @NotNull final Integer index,
                                          @NotNull final Status status) throws SQLException
    {
        @NotNull final E entity = findByIndex(userId, index);
        @NotNull final String entityId = entity.getId();
        @NotNull final String subQueryUpdate = status.compareTo(Status.IN_PROGRESS) > 0 ?
                ", start_date = current_timestamp" : "";
        @NotNull final String query = "UPDATE " + getTableName() +
                " SET status = ? " + subQueryUpdate +
                " WHERE user_id = ? and id = ?;";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, status.toString());
        statement.setString(2, userId);
        statement.setString(3, entityId);
        statement.executeUpdate();
        statement.close();
        return entity;
    }

    @Override
    public @NotNull E changeStatusByName(@NotNull final String userId,
                                         @NotNull final String name,
                                         @NotNull final Status status) throws SQLException
    {
        @NotNull final E entity = findByName(userId, name);
        @NotNull final String subQueryUpdate = status.compareTo(Status.IN_PROGRESS) > 0 ?
                ", start_date = current_timestamp" : "";
        @NotNull final String query = "UPDATE " + getTableName() +
                " SET status = ? " + subQueryUpdate +
                " WHERE user_id = ? and name = ?;";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, status.toString());
        statement.setString(2, userId);
        statement.setString(3, name);
        statement.executeUpdate();
        statement.close();
        return entity;
    }

    @NotNull
    @Override
    public E removeByName(@Nullable String userId, @Nullable String name) throws SQLException {
        @NotNull final E entity = findByName(userId, name);
        @NotNull final String query = "DELETE FROM " + getTableName() +
                " WHERE user_id = ? and name = ?;";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, userId);
        statement.setString(2, name);
        statement.executeUpdate();
        statement.close();
        return entity;
    }

}
