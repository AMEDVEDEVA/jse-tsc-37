package ru.tsc.golovina.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.golovina.tm.api.IRepository;
import ru.tsc.golovina.tm.enumerated.Role;
import ru.tsc.golovina.tm.model.User;

import java.sql.SQLException;
import java.util.List;
import java.util.function.Predicate;

public interface IUserRepository {

    @NotNull
    static Predicate<User> predicateByEmail(@NotNull String email) {
        return u -> email.equals(u.getEmail());
    }

    @Delete("DELETE FROM users WHERE id = #{user.id}")
    void remove(@NotNull @Param("user") User user);

    @Delete("DELETE FROM users")
    void clear();

    @Nullable
    @Select("SELECT * FROM users")
    @Results(value = {
            @Result(property = "passwordHash", column = "password_hash"),
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "middleName", column = "middle_name"),
            @Result(property = "lastName", column = "last_name"),
    })
    List<User> findAll();


    @Nullable
    @Select("SELECT * FROM users WHERE id = #{id}")
    @Results(value = {
            @Result(property = "passwordHash", column = "password_hash"),
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "middleName", column = "middle_name"),
            @Result(property = "lastName", column = "last_name"),
    })
    User findById(@NotNull @Param("id") String id);

    @Nullable
    @Select("SELECT * FROM users LIMIT 1 OFFSET #{index}")
    @Results(value = {
            @Result(property = "passwordHash", column = "password_hash"),
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "middleName", column = "middle_name"),
            @Result(property = "lastName", column = "last_name"),
    })
    User findByIndex(@Param("index") int index);

    @Delete("DELETE FROM users WHERE id = #{id}")
    void removeById(@NotNull @Param("id") String id);

    @Insert("INSERT INTO users" +
            " (id, login, password_hash, email, role, first_name, last_name, middle_name, locked)" +
            " VALUES (#{user.id}, #{user.login}, #{user.passwordHash}, #{user.email}, #{user.role}," +
            " #{user.firstName}, #{user.lastName}, #{user.middleName}, #{user.locked});")
    void add(@NotNull @Param("user") User user);

    @Nullable
    @Select("SELECT * FROM users WHERE login = #{login}")
    @Results(value = {
            @Result(property = "passwordHash", column = "password_hash"),
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "middleName", column = "middle_name"),
            @Result(property = "lastName", column = "last_name"),
    })
    User findByLogin(@NotNull @Param("login") String login);

    @Delete("DELETE FROM users WHERE login = #{login}")
    void removeByLogin(@NotNull @Param("login") String login);

    @Update("UPDATE users SET password_hash = #{passwordHash} WHERE id = #{id}")
    void setPassword(
            @NotNull @Param("id") String id,
            @NotNull @Param("passwordHash") String passwordHash
    );

    @Update("UPDATE users SET role = #{role} WHERE id = #{id}")
    void setRole(
            @NotNull @Param("id") String id,
            @NotNull @Param("role") Role role
    );

    @Update("UPDATE users" +
            " SET last_name = #{lastName}, first_name = #{firstName}, middle_name = #{middleName}, email = #{email}" +
            " WHERE id = #{id}")
    void updateById(
            @NotNull @Param("id") String id,
            @NotNull @Param("lastName") String lastName,
            @NotNull @Param("firstName") String firstName,
            @NotNull @Param("middleName") String middleName,
            @NotNull @Param("email") String email
    );

    @Update("UPDATE users SET locked = TRUE WHERE login = #{login}")
    void lockByLogin(@NotNull @Param("login") String login);

    @Update("UPDATE users SET locked = FALSE WHERE login = #{login}")
    void unlockByLogin(@NotNull @Param("login") String login);

    @Nullable
    @Select("SELECT * FROM users WHERE login = #{login}")
    @Results(value = {
            @Result(property = "passwordHash", column = "password_hash"),
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "middleName", column = "middle_name"),
            @Result(property = "lastName", column = "last_name"),
    })
    User isLogin(@NotNull @Param("login") String login);

    @Nullable User findUserByLogin(@Nullable String login) throws SQLException;

    @Nullable User findUserByEmail(@NotNull String email) throws SQLException;

    @NotNull User removeUserById(@NotNull String id) throws SQLException;

    @Nullable User removeUserByLogin(@NotNull String login) throws SQLException;

    boolean userExistsByLogin(@NotNull String login) throws SQLException;

    boolean userExistsByEmail(@NotNull String email) throws SQLException;
}
