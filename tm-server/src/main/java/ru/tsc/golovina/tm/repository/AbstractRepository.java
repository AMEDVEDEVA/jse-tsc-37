package ru.tsc.golovina.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.golovina.tm.api.IRepository;
import ru.tsc.golovina.tm.constant.FieldConst;
import ru.tsc.golovina.tm.exception.AbstractException;
import ru.tsc.golovina.tm.exception.entity.EntityNotFoundException;
import ru.tsc.golovina.tm.exception.system.DatabaseException;
import ru.tsc.golovina.tm.exception.system.IndexIncorrectException;
import ru.tsc.golovina.tm.model.AbstractEntity;

import java.sql.*;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractRepository<E extends AbstractEntity> implements IRepository<E> {

    @NotNull
    protected final Connection connection;

    public AbstractRepository(@NotNull Connection connection) {
        this.connection = connection;
    }

    protected abstract @NotNull E fetch(@NotNull final ResultSet row) throws AbstractException, SQLException;

    @NotNull
    protected abstract String getTableName();

    @Override
    public void add(final @NotNull E entity) throws SQLException {
        @NotNull final String query = "INSERT INTO " + getTableName() +
                " (id, name, description, status, create_date, start_date, user_id) " +
                "VALUES (?, ?, ?, ?, ?, ?, ?);";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, entity.getId());
        statement.setString(2, entity.getName());
        statement.setString(3, entity.getDescription());
        statement.setString(4, entity.getStatus().toString());
        statement.setTimestamp(5, new Timestamp(entity.getCreateDate().getTime()));
        @Nullable final Date startDate = entity.getStartDate();
        statement.setTimestamp(6, startDate == null ? null : new Timestamp(startDate.getTime()));
        statement.setString(7, entity.getUserId());
        statement.executeUpdate();
        statement.close();
    }

    @Override
    public void addAll(final @NotNull List<E> entities) {
        list.addAll(entities);
    }

    @Override
    public void remove(final @NotNull E entity) throws SQLException {
        @NotNull final String id = entity.getId();
        @NotNull final String query = "DELETE FROM " + getTableName() + " WHERE id = ?;";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, id);
        statement.execute();
        statement.close();
    }

    @Override
    public @NotNull List<E> findAll() {
        @NotNull final String query = "SELECT * FROM " + getTableName() + ";";
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(query)) {
            @NotNull final ResultSet resultSet = statement.executeQuery();
            final List<E> result = new ArrayList<>();
            while (resultSet.next()) result.add(fetch(resultSet));
            return result;
        } catch (SQLException exception) {
            throw new DatabaseException();
        }
    }

    @Override
    public @NotNull List<E> findAll(final @NotNull Comparator<E> comparator) {
        final List<E> entitiesList = findAll();
        entitiesList.sort(comparator);
        return entitiesList;
    }

    @Override
    public boolean existsById(@NotNull final String id) throws SQLException {
        @NotNull final String query = "SELECT COUNT(*) FROM " + getTableName() + " WHERE id = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, id);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        if (!resultSet.next()) return false;
        return resultSet.getInt(1) >= 1;
    }

    @Override
    public boolean existsByIndex(@NotNull final Integer index) throws SQLException {
        @NotNull final String query = "SELECT COUNT(*) FROM " + getTableName() + " LIMIT 1 OFFSET ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setInt(1, index);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        if (!resultSet.next()) return false;
        return resultSet.getInt(1) >= 1;
    }

    @Override
    public void clear() throws SQLException {
        @NotNull final String query = "DELETE FROM " + getTableName() + ";";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.execute();
        statement.close();
    }

    @Override
    public @NotNull E findById(@NotNull final String id) throws SQLException {
        @NotNull final String query = "SELECT * FROM " + getTableName() + " WHERE id = ?;";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, id);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        if (!resultSet.next()) throw new EntityNotFoundException();
        final @NotNull E entity = fetch(resultSet);
        statement.close();
        return entity;
    }

    @Override
    public @Nullable E findByIndex(@NotNull final Integer index) throws SQLException {
        if (index < 0) throw new IndexIncorrectException();
        @NotNull final String query = "SELECT * FROM " + getTableName() + " LIMIT 1 OFFSET ?;";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setInt(1, index);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        if (!resultSet.next()) throw new IndexIncorrectException();
        final @NotNull E entity = fetch(resultSet);
        statement.close();
        return entity;
    }

    @Override
    public @Nullable E removeById(@NotNull final String id) throws SQLException {
        @NotNull final String query = "DELETE FROM " + getTableName() + " WHERE id = ?;";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, id);
        statement.executeUpdate();
        statement.close();
    }

    @Override
    public @Nullable E removeByIndex(@NotNull final Integer index) throws SQLException {
        @NotNull final E entity = findByIndex(index);
        remove(entity);
        return entity;
    }

    @NotNull
    @Override
    public Integer getSize() throws SQLException {
        @NotNull final String query = "SELECT COUNT(*) FROM " + getTableName();
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        statement.close();
        return resultSet.getInt("total");
    }

}
