package ru.tsc.golovina.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.golovina.tm.api.repository.ITaskRepository;
import ru.tsc.golovina.tm.api.service.IConnectionService;
import ru.tsc.golovina.tm.api.service.ITaskService;
import ru.tsc.golovina.tm.enumerated.Status;
import ru.tsc.golovina.tm.exception.empty.*;
import ru.tsc.golovina.tm.exception.system.DatabaseException;
import ru.tsc.golovina.tm.exception.system.IndexIncorrectException;
import ru.tsc.golovina.tm.model.Task;
import ru.tsc.golovina.tm.repository.TaskRepository;

import java.sql.Connection;
import java.sql.SQLException;

public final class TaskService extends AbstractOwnerService<Task> implements ITaskService {

    public TaskService(@NotNull IConnectionService connectionService) {
        super(connectionService);
    }

    @NotNull
    @Override
    public ITaskRepository getRepository(@NotNull final Connection connection) {
        return new TaskRepository(connection);
    }

    @Override
    public Task create(@Nullable final String userId, @Nullable final String name, @Nullable final String description) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();
        @NotNull Task task = new Task();
        task.setUserId(userId);
        task.setName(name);
        task.setDescription(description);
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final ITaskRepository taskRepository = getRepository(connection);
            taskRepository.add(task);
        } catch (SQLException exception) {
            throw new DatabaseException();
        }
        return task;
    }

}