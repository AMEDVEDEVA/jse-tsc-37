package ru.tsc.golovina.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.golovina.tm.enumerated.Status;
import ru.tsc.golovina.tm.model.Project;
import ru.tsc.golovina.tm.model.Session;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import java.util.List;

public interface IProjectEndpoint {

    @WebMethod
    @NotNull List<Project> findAllByUserId(
            @Nullable @WebParam(name = "session") Session session
    );

    @WebMethod
    @NotNull Project findByIdUserId(
            @Nullable @WebParam(name = "session") Session session,
            @Nullable @WebParam(name = "id") String id
    );

    @WebMethod
    @NotNull Project findByIndexUserId(
            @Nullable @WebParam(name = "session") Session session,
            @Nullable @WebParam(name = "index") Integer index
    );

    @WebMethod
    void clearByUserId(
            @Nullable @WebParam(name = "session") Session session
    );

    @WebMethod
    void removeByUserId(
            @Nullable @WebParam(name = "session") Session session,
            @Nullable @WebParam(name = "entity") Project entity
    );

    @WebMethod
    @NotNull Integer getSizeByUserId(
            @Nullable @WebParam(name = "session") Session session
    );

    @WebMethod
    void add(@Nullable @WebParam(name = "session") Session session,
             @Nullable @WebParam(name = "entity") Project entity
    );

    @WebMethod
    void addAll(
            @Nullable @WebParam(name = "session") Session session,
            @Nullable @WebParam(name = "entities", partName = "entities") List<Project> entities
    );

    @WebMethod
    void remove(
            @Nullable @WebParam(name = "session") Session session,
            @Nullable @WebParam(name = "entity") Project entity
    );

    @WebMethod
    @NotNull List<Project> findAll(@Nullable @WebParam(name = "session") Session session);

    @WebMethod
    void clear(@Nullable @WebParam(name = "session") Session session);

    @WebMethod
    @NotNull Project findById(
            @Nullable @WebParam(name = "session") Session session,
            @Nullable @WebParam(name = "id") String id
    );

    @WebMethod
    @NotNull Project findByIndex(
            @Nullable @WebParam(name = "session") Session session,
            @NotNull @WebParam(name = "index") Integer index
    );

    @WebMethod
    boolean existsById(
            @Nullable @WebParam(name = "session") Session session,
            @Nullable @WebParam(name = "id") String id
    );

    @WebMethod
    boolean existsByIndex(
            @Nullable @WebParam(name = "session") Session session,
            @Nullable @WebParam(name = "index") Integer index
    );

    @WebMethod
    @NotNull Project removeById(
            @Nullable @WebParam(name = "session") Session session,
            @NotNull @WebParam(name = "id") String id
    );

    @WebMethod
    @NotNull Project removeByIndex(
            @Nullable @WebParam(name = "session") Session session,
            @Nullable @WebParam(name = "index") Integer index
    );

    @WebMethod
    @NotNull Integer getSize(
            @Nullable @WebParam(name = "session") Session session);

    @WebMethod
    void create(
            @Nullable @WebParam(name = "session") Session session,
            @Nullable @WebParam(name = "name") String name
    );

    @WebMethod
    void createWithDescription(
            @Nullable @WebParam(name = "session") Session session,
            @Nullable @WebParam(name = "name") String name,
            @Nullable @WebParam(name = "description") String description
    );

    @WebMethod
    @NotNull Project findByName(
            @Nullable @WebParam(name = "session") Session session,
            @Nullable @WebParam(name = "name") String name
    );

    @WebMethod
    void updateById(
            @Nullable @WebParam(name = "session") Session session,
            @Nullable @WebParam(name = "id") String id,
            @Nullable @WebParam(name = "name") String name,
            @NotNull @WebParam(name = "description") String description);

    @WebMethod
    void updateByIndex(
            @Nullable @WebParam(name = "session") Session session,
            @Nullable @WebParam(name = "index") Integer index,
            @Nullable @WebParam(name = "name") String name,
            @NotNull @WebParam(name = "description") String description
    );

    @WebMethod
    boolean existsByName(
            @Nullable @WebParam(name = "session") Session session,
            @NotNull @WebParam(name = "name") String name
    );

    @WebMethod
    @NotNull Project startById(
            @Nullable @WebParam(name = "session") Session session,
            @Nullable @WebParam(name = "id") String id
    );

    @WebMethod
    @NotNull Project startByIndex(
            @Nullable @WebParam(name = "session") Session session,
            @Nullable @WebParam(name = "index") Integer index
    );

    @WebMethod
    @NotNull Project startByName(
            @Nullable @WebParam(name = "session") Session session,
            @Nullable @WebParam(name = "name") String name
    );

    @WebMethod
    @NotNull Project finishById(
            @Nullable @WebParam(name = "session") Session session,
            @Nullable @WebParam(name = "id") String id
    );

    @WebMethod
    @NotNull Project finishByIndex(
            @Nullable @WebParam(name = "session") Session session,
            @Nullable @WebParam(name = "index") Integer index
    );

    @WebMethod
    @NotNull Project finishByName(
            @Nullable @WebParam(name = "session") Session session,
            @Nullable @WebParam(name = "name") String name
    );

    @WebMethod
    @NotNull Project changeStatusById(
            @Nullable @WebParam(name = "session") Session session,
            @Nullable @WebParam(name = "id") String id,
            @Nullable @WebParam(name = "status") Status status
    );

    @WebMethod
    @NotNull Project changeStatusByIndex(
            @Nullable @WebParam(name = "session") Session session,
            @Nullable @WebParam(name = "index") Integer index,
            @Nullable @WebParam(name = "status") Status status
    );

    @WebMethod
    @NotNull Project changeStatusByName(
            @Nullable @WebParam(name = "session") Session session,
            @Nullable @WebParam(name = "name") String name,
            @Nullable @WebParam(name = "status") Status status
    );

}
