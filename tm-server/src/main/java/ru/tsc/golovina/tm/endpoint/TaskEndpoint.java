package ru.tsc.golovina.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.golovina.tm.api.endpoint.ITaskEndpoint;
import ru.tsc.golovina.tm.api.service.ISessionService;
import ru.tsc.golovina.tm.api.service.ITaskService;
import ru.tsc.golovina.tm.enumerated.Status;
import ru.tsc.golovina.tm.model.Session;
import ru.tsc.golovina.tm.model.Task;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public class TaskEndpoint implements ITaskEndpoint {

    private ITaskService taskService;

    private ISessionService sessionService;

    public TaskEndpoint() {
    }

    public TaskEndpoint(final ITaskService taskService, ISessionService sessionService) {
        this.taskService = taskService;
        this.sessionService = sessionService;
    }

    @Override
    @WebMethod
    public void add(
            @Nullable @WebParam(name = "session") Session session,
            @NotNull @WebParam(name = "entity") Task entity
    ) {
        sessionService.validate(session);
        taskService.add(session.getUserId(), entity);
    }

    @Override
    @WebMethod
    public void addAll(
            @Nullable @WebParam(name = "session") Session session,
            @NotNull @WebParam(name = "entities") List<Task> entities
    ) {
        sessionService.validate(session);
        taskService.addAll(session.getUserId(), entities);
    }

    @Override
    @WebMethod
    public void remove(
            @Nullable @WebParam(name = "session") Session session,
            @NotNull @WebParam(name = "entity") Task entity
    ) {
        sessionService.validate(session);
        taskService.remove(session.getUserId(), entity);
    }

    @Override
    @WebMethod
    public @NotNull List<Task> findAll(@Nullable @WebParam(name = "session") Session session) {
        sessionService.validate(session);
        return taskService.findAll(session.getUserId());
    }

    @Override
    @WebMethod
    public boolean existsById(
            @Nullable @WebParam(name = "session") Session session,
            @NotNull @WebParam(name = "id") String id
    ) {
        sessionService.validate(session);
        return taskService.existsById(session.getUserId(), id);
    }

    @Override
    @WebMethod
    public boolean existsByIndex(
            @Nullable @WebParam(name = "session") Session session,
            @NotNull @WebParam(name = "index") Integer index
    ) {
        sessionService.validate(session);
        return taskService.existsByIndex(session.getUserId(), index);
    }

    @Override
    @WebMethod
    public void clear(@Nullable @WebParam(name = "session") Session session) {
        sessionService.validate(session);
        taskService.clear(session.getUserId());
    }

    @Override
    @WebMethod
    public @NotNull Task findById(
            @Nullable @WebParam(name = "session") Session session,
            @NotNull @WebParam(name = "id") String id
    ) {
        sessionService.validate(session);
        return taskService.findById(session.getUserId(), id);
    }

    @Override
    @WebMethod
    public @Nullable Task findByIndex(
            @Nullable @WebParam(name = "session") Session session,
            @NotNull @WebParam(name = "index") Integer index
    ) {
        sessionService.validate(session);
        return taskService.findByIndex(session.getUserId(), index);
    }

    @Override
    @WebMethod
    public @Nullable Task removeById(
            @Nullable @WebParam(name = "session") Session session,
            @NotNull @WebParam(name = "id") String id
    ) {
        sessionService.validate(session);
        return taskService.removeById(session.getUserId(), id);
    }

    @Override
    @WebMethod
    public @Nullable Task removeByIndex(
            @Nullable @WebParam(name = "session") Session session,
            @NotNull @WebParam(name = "index") Integer index
    ) {
        sessionService.validate(session);
        return taskService.removeByIndex(session.getUserId(), index);
    }

    @Override
    @WebMethod
    public @NotNull List<Task> findAllByUserId(@Nullable @WebParam(name = "session") Session session) {
        sessionService.validate(session);
        return taskService.findAll(session.getUserId());
    }

    @Override
    @WebMethod
    public @NotNull Task findByIdUSerId(
            @Nullable @WebParam(name = "session") Session session,
            @Nullable @WebParam(name = "id") String id
    ) {
        sessionService.validate(session);
        return taskService.findById(session.getUserId(), id);
    }

    @Override
    @WebMethod
    public @NotNull Task findByIndexUserId(
            @Nullable @WebParam(name = "session") Session session,
            @Nullable @WebParam(name = "index") Integer index
    ) {
        sessionService.validate(session);
        return taskService.findByIndex(session.getUserId(), index);
    }

    @Override
    @WebMethod
    public void clearByUserId(@Nullable @WebParam(name = "session") Session session) {
        sessionService.validate(session);
        taskService.clear(session.getUserId());
    }

    @Override
    @WebMethod
    public void removeByUserId(
            @Nullable @WebParam(name = "session") Session session,
            @Nullable @WebParam(name = "entity") Task entity
    ) {
        sessionService.validate(session);
        taskService.remove(session.getUserId(), entity);
    }

    @Override
    @WebMethod
    public @NotNull Integer getSize(@Nullable @WebParam(name = "session") Session session) {
        sessionService.validate(session);
        return taskService.getSize(session.getUserId());
    }

    @Override
    @WebMethod
    public void create(
            @Nullable @WebParam(name = "session") Session session,
            @Nullable @WebParam(name = "name") String name
    ) {
        sessionService.validate(session);
        taskService.create(session.getUserId(), name);
    }

    @Override
    @WebMethod
    public void create(
            @Nullable @WebParam(name = "session") Session session,
            @Nullable @WebParam(name = "name") String name,
            @Nullable @WebParam(name = "description") String description
    ) {
        sessionService.validate(session);
        taskService.create(session.getUserId(), name, description);
    }

    @Override
    @WebMethod
    public @NotNull Task removeByName(
            @Nullable @WebParam(name = "session") Session session,
            @Nullable @WebParam(name = "name") String name
    ) {
        sessionService.validate(session);
        return taskService.removeByName(session.getUserId(), name);
    }

    @Override
    @WebMethod
    public void updateById(
            @Nullable @WebParam(name = "session") Session session,
            @Nullable @WebParam(name = "id") String id,
            @Nullable @WebParam(name = "name") String name,
            @Nullable @WebParam(name = "description") String description
    ) {
        sessionService.validate(session);
        taskService.updateById(session.getUserId(), id, name, description);
    }

    @Override
    @WebMethod
    public void updateByIndex(
            @Nullable @WebParam(name = "session") Session session,
            @Nullable @WebParam(name = "index") Integer index,
            @Nullable @WebParam(name = "name") String name,
            @Nullable @WebParam(name = "description") String description
    ) {
        sessionService.validate(session);
        taskService.updateByIndex(session.getUserId(), index, name, description);
    }

    @Override
    @WebMethod
    public boolean existsByName(
            @Nullable @WebParam(name = "session") Session session,
            @NotNull @WebParam(name = "name") String name
    ) {
        sessionService.validate(session);
        return taskService.existsByName(session.getUserId(), name);
    }

    @Override
    @WebMethod
    public @NotNull Task findByName(
            @Nullable @WebParam(name = "session") Session session,
            @Nullable @WebParam(name = "name") String name
    ) {
        sessionService.validate(session);
        return taskService.findByName(session.getUserId(), name);
    }

    @Override
    @WebMethod
    public @NotNull Task startById(
            @Nullable @WebParam(name = "session") Session session,
            @Nullable @WebParam(name = "id") String id
    ) {
        sessionService.validate(session);
        return taskService.startById(session.getUserId(), id);
    }

    @Override
    @WebMethod
    public @NotNull Task startByIndex(
            @Nullable @WebParam(name = "session") Session session,
            @Nullable @WebParam(name = "index") Integer index
    ) {
        sessionService.validate(session);
        return taskService.startByIndex(session.getUserId(), index);
    }

    @Override
    @WebMethod
    public @NotNull Task startByName(
            @Nullable @WebParam(name = "session") Session session,
            @Nullable @WebParam(name = "name") String name
    ) {
        sessionService.validate(session);
        return taskService.startByName(session.getUserId(), name);
    }

    @Override
    @WebMethod
    public @NotNull Task finishById(
            @Nullable @WebParam(name = "session") Session session,
            @Nullable @WebParam(name = "id") String id
    ) {
        sessionService.validate(session);
        return taskService.finishById(session.getUserId(), id);
    }

    @Override
    @WebMethod
    public @NotNull Task finishByIndex(
            @Nullable @WebParam(name = "session") Session session,
            @Nullable @WebParam(name = "index") Integer index
    ) {
        sessionService.validate(session);
        return taskService.finishByIndex(session.getUserId(), index);
    }

    @Override
    @WebMethod
    public @NotNull Task finishByName(
            @Nullable @WebParam(name = "session") Session session,
            @Nullable @WebParam(name = "name") String name
    ) {
        sessionService.validate(session);
        return taskService.finishByName(session.getUserId(), name);
    }

    @Override
    @WebMethod
    public @NotNull Task changeStatusById(
            @Nullable @WebParam(name = "session") Session session,
            @Nullable @WebParam(name = "id") String id,
            @Nullable @WebParam(name = "status") Status status
    ) {
        sessionService.validate(session);
        return taskService.changeStatusById(session.getUserId(), id, status);
    }

    @Override
    @WebMethod
    public @NotNull Task changeStatusByIndex(
            @Nullable @WebParam(name = "session") Session session,
            @Nullable @WebParam(name = "index") Integer index,
            @Nullable @WebParam(name = "status") Status status
    ) {
        sessionService.validate(session);
        return taskService.changeStatusByIndex(session.getUserId(), index, status);
    }

    @Override
    @WebMethod
    public @NotNull Task changeStatusByName(
            @Nullable @WebParam(name = "session") Session session,
            @Nullable @WebParam(name = "name") String name,
            @Nullable @WebParam(name = "status") Status status
    ) {
        sessionService.validate(session);
        return taskService.changeStatusByName(session.getUserId(), name, status);
    }

}
