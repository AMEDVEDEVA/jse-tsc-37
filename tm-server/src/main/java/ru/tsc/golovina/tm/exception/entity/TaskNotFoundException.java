package ru.tsc.golovina.tm.exception.entity;

import ru.tsc.golovina.tm.exception.AbstractException;

public class TaskNotFoundException extends AbstractException {

    public TaskNotFoundException() {
        super("Error. Task not found.");
    }

}
