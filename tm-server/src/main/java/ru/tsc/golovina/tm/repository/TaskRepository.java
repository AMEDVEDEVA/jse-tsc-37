package ru.tsc.golovina.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.tsc.golovina.tm.api.repository.ITaskRepository;
import ru.tsc.golovina.tm.exception.AbstractException;
import ru.tsc.golovina.tm.exception.entity.TaskNotFoundException;
import ru.tsc.golovina.tm.model.Task;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public final class TaskRepository extends AbstractOwnerRepository<Task> implements ITaskRepository {

    public TaskRepository(@NotNull Connection connection) {
        super(connection);
    }

    @Override
    protected @NotNull Task fetch(@NotNull ResultSet row) throws AbstractException, SQLException {
        @NotNull final Task project = new Task();
        project.setName(row.getString("name"));
        project.setDescription(row.getString("description"));
        project.setId(row.getString("id"));
        project.setUserId(row.getString("user_id"));
        project.setStartDate(row.getDate("start_date"));
        project.setFinishDate(row.getDate("finish_date"));
        project.setCreateDate(row.getDate("created_date"));
        project.setProjectId(row.getString("project_id"));
        return project;
    }

    protected String getTableName() {
        return "task";
    }

    @NotNull
    @Override
    public List<Task> findAllTaskByProjectId(
            @NotNull final String userId,
            @NotNull final String projectId) throws SQLException
    {
        @NotNull final String query = "SELECT * FROM " + getTableName() +
                " WHERE user_id = ? AND project_id = ?;";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, userId);
        statement.setString(2, projectId);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        @NotNull final List<Task> result = new ArrayList<>();
        while (resultSet.next()) result.add(fetch(resultSet));
        statement.close();
        if (result.isEmpty()) throw new TaskNotFoundException();
        return result;

    }

    @Override
    public void bindTaskToProjectById(@NotNull final String userId,
                                      @NotNull final String projectId,
                                      @NotNull final String taskId) throws SQLException
    {
        @NotNull final String query = "UPDATE " + getTableName() +
                " SET project_id = ?" +
                " WHERE user_id = ? and id = ?;";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, projectId);
        statement.setString(2, userId);
        statement.setString(3, taskId);
        statement.execute();
        statement.close();
    }

    @Override
    public void unbindTaskById(@NotNull final String userId, @NotNull final String taskId) throws SQLException {
        @NotNull final String query = "UPDATE " + getTableName() +
                " SET project_id = ?" +
                " WHERE user_id = ? and id = ?;";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setNull(1, Types.VARCHAR);
        statement.setString(2, userId);
        statement.setString(3, taskId);
        statement.executeUpdate();
        statement.close();
    }

    @Override
    public void removeAllTaskByProjectId(@NotNull final String userId,
                                         @NotNull final String projectId) throws SQLException
    {
        @NotNull final String query = "UPDATE " + getTableName() +
                " SET project_id = ?" +
                " WHERE user_id = ? and project_id = ?;";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setNull(1, Types.VARCHAR);
        statement.setString(2, userId);
        statement.setString(3, projectId);
        statement.executeUpdate();
        statement.close();
    }

}
