package ru.tsc.golovina.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.golovina.tm.model.Task;

import java.sql.SQLException;
import java.util.List;

public interface ITaskRepository {

    @Nullable
    @Select("SELECT * FROM tasks WHERE user_id = #{userId} AND id = #{id}")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id"),
            @Result(property = "startDate", column = "start_date"),
            @Result(property = "createDate", column = "create_date")
    })
    Task existById(@NotNull @Param("userId") String userId, @NotNull @Param("id") String id);

    @Delete("DELETE FROM tasks WHERE user_id = #{userId} AND id = #{task.id};")
    void remove(@NotNull @Param("userId") String userId, @NotNull @Param("task") Task task);

    @Nullable
    @Select("SELECT * FROM tasks WHERE user_id = #{userId};")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id"),
            @Result(property = "startDate", column = "start_date"),
            @Result(property = "createDate", column = "create_date")
    })
    List<Task> findAllByUserId(@NotNull @Param("userId") String userId);

    @Nullable
    @Select("SELECT * FROM tasks WHERE user_id = #{userId} AND id = #{id};")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id"),
            @Result(property = "startDate", column = "start_date"),
            @Result(property = "createDate", column = "create_date")
    })
    Task findByUserIdAndTaskId(@NotNull @Param("userId") String userId, @NotNull @Param("id") String id);

    @Delete("DELETE FROM tasks WHERE user_id = #{userId} AND id = #{id}")
    void removeByUserIdAndTaskId(@NotNull @Param("userId") String userId, @NotNull @Param("id") String id);

    @Insert("INSERT INTO tasks" +
            " (id, name, description, status, create_date, start_date, user_id, project_id)" +
            " VALUES (#{task.id}, #{task.name}, #{task.description}," +
            " #{task.status}, #{task.createDate}, #{task.startDate}, #{task.userId}, #{task.projectId})")
    void add(@NotNull @Param("task") Task task);

    @Delete("DELETE FROM tasks WHERE user_id = #{userId}")
    void clearById(@NotNull @Param("userId") String userId);

    @Nullable
    @Select("SELECT * FROM tasks" +
            " WHERE user_id = #{userId} AND name = #{name}")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id"),
            @Result(property = "startDate", column = "start_date"),
            @Result(property = "createDate", column = "create_date")
    })
    Task findByName(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("name") String name
    );

    @Nullable
    @Select("SELECT * FROM tasks" +
            " WHERE user_id = #{userId} LIMIT 1 OFFSET #{index}")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id"),
            @Result(property = "startDate", column = "start_date"),
            @Result(property = "createDate", column = "create_date")
    })
    Task findByIndex(
            @NotNull @Param("userId") String userId,
            @Param("index") int index
    );

    @Delete("DELETE FROM tasks WHERE user_id = #{userId} AND name = #{name}")
    void removeByName(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("name") String name
    );

    @Update("UPDATE tasks SET name = #{name}, description = #{description}" +
            " WHERE user_id = #{userId} AND id = #{id}")
    void updateById(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("id") String id,
            @NotNull @Param("name") String name,
            @NotNull @Param("description") String description
    );

    @Update("UPDATE tasks SET name = #{name}, description = #{description}" +
            " WHERE user_id = #{userId} AND id = #{id};")
    void updateByIndex(
            @NotNull @Param("userId") String userId,
            @Param("id") String id,
            @NotNull @Param("name") String name,
            @NotNull @Param("description") String description
    );

    @Update("UPDATE tasks SET status = #{status}, start_date = CURRENT_TIMESTAMP" +
            " WHERE user_id = #{userId} AND id = #{id};")
    void startById(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("id") String id,
            @NotNull @Param("status") String status
    );

    @Update("UPDATE tasks SET status = #{status}, start_date = CURRENT_TIMESTAMP" +
            " WHERE user_id = #{userId} AND id = #{id}")
    void startByIndex(
            @NotNull @Param("userId") String userId,
            @Param("id") String id,
            @NotNull @Param("status") String status
    );

    @Update("UPDATE tasks SET status = #{status}, start_date = CURRENT_TIMESTAMP" +
            " WHERE user_id = #{userId} AND name = #{name};")
    void startByName(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("name") String name,
            @NotNull @Param("status") String status
    );

    @Update("UPDATE tasks SET status = #{status} WHERE user_id = #{userId} AND id = #{id};")
    void finishById(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("id") String id,
            @NotNull @Param("status") String status
    );

    @Update("UPDATE tasks SET status = #{status} WHERE user_id = #{userId} AND id = #{id}")
    void finishByIndex(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("id") String id,
            @NotNull @Param("status") String status
    );

    @Update("UPDATE tasks SET status = #{status} WHERE user_id = #{userId} AND name = #{name};")
    void finishByName(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("name") String name,
            @NotNull @Param("status") String status
    );

    @Update("UPDATE tasks SET status = #{status} WHERE user_id = #{userId} AND id = #{id};")
    void updateStatusById(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("id") String id,
            @NotNull @Param("status") String status
    );

    @Update("UPDATE tasks SET status = #{status} WHERE user_id = #{userId} AND id = #{id};")
    void updateStatusByIndex(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("id") String id,
            @NotNull @Param("status") String status
    );

    @Update("UPDATE tasks SET status = #{status} WHERE user_id = #{userId} AND name = #{name};")
    void updateStatusByName(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("name") String name,
            @NotNull @Param("status") String status
    );

    @NotNull List<Task> findAllTaskByProjectId(
            @NotNull String userId,
            @NotNull String projectId) throws SQLException;

    @Update("UPDATE tasks SET project_id = #{projectId} WHERE user_id = #{userId} and id = #{taskId};")
    void bindTaskToProjectById(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("projectId") String projectId,
            @NotNull @Param("taskId") String taskId
    ) throws SQLException;

    @Update("UPDATE tasks SET project_id = NULL WHERE user_id = #{userId} and id = #{id};")
    void unbindTaskById(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("id") String id
    ) throws SQLException;

    @Nullable
    @Select("SELECT * FROM tasks WHERE user_id = #{userId} AND project_id = #{id};")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id"),
            @Result(property = "startDate", column = "start_date"),
            @Result(property = "createDate", column = "create_date")
    })
    List<Task> findAllByProjectId(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("id") String id
    );

    @Update("UPDATE tasks SET project_id = NULL WHERE user_id = #{userId} and project_id = #{id};")
    void removeAllTaskByProjectId(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("id") String id
    ) throws SQLException;

    @Delete("DELETE FROM tasks")
    void clear();

    @Nullable
    @Select("SELECT * FROM tasks")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id"),
            @Result(property = "startDate", column = "start_date"),
            @Result(property = "createDate", column = "create_date")
    })
    List<Task> findAll();

    @Nullable
    @Select("SELECT * FROM tasks WHERE id = #{id};")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id"),
            @Result(property = "startDate", column = "start_date"),
            @Result(property = "createDate", column = "create_date")
    })
    Task findById(@NotNull @Param("id") String id);

    @Delete("DELETE FROM tasks id = #{id}")
    void removeById(@NotNull @Param("id") String id);

}