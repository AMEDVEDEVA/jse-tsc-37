package ru.tsc.golovina.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.tsc.golovina.tm.api.IPropertyService;
import ru.tsc.golovina.tm.api.service.IConnectionService;

import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Properties;

public class ConnectionService implements IConnectionService {

    @NotNull
    private final IPropertyService propertyService;

    public ConnectionService(@NotNull IPropertyService propertyService) {
        this.propertyService = propertyService;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Connection getConnection() {
        @NotNull final String url = propertyService.getJdbcUrl();
        @NotNull final Properties properties = new Properties();
        properties.setProperty("user", propertyService.getJdbcLogin());
        properties.setProperty("password", propertyService.getJdbcPassword());
        @NotNull final Connection connection = DriverManager.getConnection(url, properties);
        connection.setAutoCommit(false);
        return connection;
    }

}
