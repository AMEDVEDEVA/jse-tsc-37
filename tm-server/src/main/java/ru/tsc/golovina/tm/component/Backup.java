package ru.tsc.golovina.tm.component;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.tsc.golovina.tm.api.IPropertyService;
import ru.tsc.golovina.tm.api.service.IDataService;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;

public class Backup implements Runnable {

    public static final String FILE_BINARY = "./data.bin";

    public static final String FILE_BASE64 = "./data.base64";

    public static final String FILE_FASTERXML_JSON = "./data-fasterxml.json";

    public static final String FILE_FASTERXML_XML = "./data-fasterxml.xml";

    public static final String FILE_FASTERXML_YAML = "./data-fasterxml.yaml";

    public static final String FILE_JAXB_JSON = "./data-jaxb.json";

    public static final String FILE_JAXB_XML = "./data-jaxb.xml";

    public static final String JAXB_JSON_PROPERTY_NAME = "eclipselink.media-type";

    public static final String JAXB_JSON_PROPERTY_VALUE = "application/json";

    public static final String SYSTEM_JSON_PROPERTY_NAME = "javax.xml.bind.context.factory";

    public static final String SYSTEM_JSON_PROPERTY_VALUE = "org.eclipse.persistence.jaxb.JAXBContextFactory";

    public static final String BACKUP_XML = "./backup.xml";

    private final ThreadFactory threadFactory = runnable -> new Thread(runnable, "DataThread");

    private final ScheduledExecutorService es = Executors.newSingleThreadScheduledExecutor(threadFactory);

    private final Bootstrap bootstrap;

    private final int INTERVAL;

    private IPropertyService propertyService;

    private IDataService dataService;

    public Backup(
            @NotNull final Bootstrap bootstrap,
            @NotNull final IPropertyService propertyService,
            @NotNull final IDataService dataService
    ) {
        this.bootstrap = bootstrap;
        this.INTERVAL = propertyService.getBackupInterval();
        this.dataService = dataService;
    }

    public void init() {
        load();
        start();
    }

    @SneakyThrows
    public void load() {
        dataService.loadBackup();
    }

    public void start() {
        es.scheduleWithFixedDelay(this, 0, INTERVAL, TimeUnit.SECONDS);
    }

    public void stop() {
        es.shutdown();
    }

    @Override
    @SneakyThrows
    public void run() {
        dataService.saveBackup();
    }

}
