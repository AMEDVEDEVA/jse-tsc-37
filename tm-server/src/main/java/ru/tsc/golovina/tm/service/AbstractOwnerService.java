package ru.tsc.golovina.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.golovina.tm.api.IRepository;
import ru.tsc.golovina.tm.api.repository.IOwnerRepository;
import ru.tsc.golovina.tm.api.service.IConnectionService;
import ru.tsc.golovina.tm.api.service.IOwnerService;
import ru.tsc.golovina.tm.enumerated.Status;
import ru.tsc.golovina.tm.exception.empty.*;
import ru.tsc.golovina.tm.exception.entity.EntityNotFoundException;
import ru.tsc.golovina.tm.exception.system.DatabaseException;
import ru.tsc.golovina.tm.exception.system.IndexIncorrectException;
import ru.tsc.golovina.tm.model.AbstractOwnerEntity;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractOwnerService<E extends AbstractOwnerEntity> extends AbstractService<E>
        implements IOwnerService<E> {

    @NotNull
    protected abstract IOwnerRepository<E> getRepository(@NotNull final Connection connection);

    public AbstractOwnerService(@NotNull IConnectionService connectionService) {
        super(connectionService);
    }

    @NotNull
    @Override
    public List<E> findAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final IOwnerRepository<E> repository = getRepository(connection);
            return repository.findAll(userId);
        } catch (SQLException exception) {
            throw new DatabaseException();
        }
    }

    @NotNull
    @Override
    public List<E> findAll(@Nullable final String userId, @Nullable final Comparator<E> comparator) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (comparator == null) return Collections.emptyList();
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final IOwnerRepository<E> repository = getRepository(connection);
            return repository.findAll(userId, comparator);
        } catch (SQLException exception) {
            throw new DatabaseException();
        }
    }

    @Override
    public void add(@Nullable String userId, @NotNull E entity) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final IOwnerRepository<E> repository = getRepository(connection);
            repository.add(userId, entity);
        } catch (SQLException exception) {
            throw new DatabaseException();
        }
    }

    @Override
    public void addAll(@Nullable String userId, @NotNull List<E> entities) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final IOwnerRepository<E> repository = getRepository(connection);
            for(E entity : entities)
                repository.add(userId, entity);
        } catch (SQLException exception) {
            throw new DatabaseException();
        }
    }

    @Override
    public boolean existsById(@Nullable String userId, @NotNull String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final IOwnerRepository<E> repository = getRepository(connection);
            return findById(userId, id) != null;
        } catch (SQLException exception) {
            throw new DatabaseException();
        }
    }

    @Override
    public boolean existsByIndex(@Nullable String userId, @NotNull Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final IOwnerRepository<E> repository = getRepository(connection);
            return findByIndex(userId, index) != null;
        } catch (SQLException exception) {
            throw new DatabaseException();
        }
    }

    @NotNull
    @Override
    public E findById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @Nullable final E entity;
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final IOwnerRepository<E> repository = getRepository(connection);
            entity = repository.findById(userId, id);
        } catch (SQLException exception) {
            throw new DatabaseException();
        }
        if (entity == null) throw new EntityNotFoundException();
        return entity;
    }

    @NotNull
    @Override
    public E findByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null) throw new EmptyIndexException();
        if (index < 0 || index > getSize(userId)) throw new IndexIncorrectException();
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final IOwnerRepository<E> repository = getRepository(connection);
            return repository.findByIndex(userId, index);
        } catch (SQLException exception) {
            throw new DatabaseException();
        }
    }

    @Override
    public void clear(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final IOwnerRepository<E> repository = getRepository(connection);
            repository.clear(userId);
        } catch (SQLException exception) {
            throw new DatabaseException();
        }
    }

    public void remove(@NotNull final String userId, @Nullable final E entity) {
        if (entity == null) throw new EntityNotFoundException();
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final IOwnerRepository<E> repository = getRepository(connection);
            repository.remove(userId, entity);
        } catch (SQLException exception) {
            throw new DatabaseException();
        }
    }

    @NotNull
    @Override
    public Integer getSize(@NotNull final String userId) {
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final IOwnerRepository<E> repository = getRepository(connection);
            return repository.getSize(userId);
        } catch (SQLException exception) {
            throw new DatabaseException();
        }
    }

    @Override
    public E create(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final E entity = create(userId, name);
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final IOwnerRepository<E> repository = getRepository(connection);
            repository.add(entity);
        } catch (SQLException exception) {
            throw new DatabaseException();
        }
        return entity;
    }

    @NotNull
    @Override
    public E findByName(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final IOwnerRepository<E> repository = getRepository(connection);
            return repository.findByName(userId, name);
        } catch (SQLException exception) {
            throw new DatabaseException();
        }
    }

    @NotNull
    @Override
    public E removeByName(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final IOwnerRepository<E> repository = getRepository(connection);
            return repository.removeByName(userId, name);
        } catch (SQLException exception) {
            throw new DatabaseException();
        }
    }

    @Override
    public @NotNull E removeById(@Nullable String userId, @NotNull String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final IOwnerRepository<E> repository = getRepository(connection);
            return repository.removeById(userId, id);
        } catch (SQLException exception) {
            throw new DatabaseException();
        }
    }

    @Override
    public @NotNull E removeByIndex(@Nullable String userId, @NotNull Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        @Nullable final E entity;
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final IOwnerRepository<E> repository = getRepository(connection);
            entity = repository.removeByIndex(userId, index);
        } catch (SQLException exception) {
            throw new DatabaseException();
        }
        if (entity == null) throw new EntityNotFoundException();
        return entity;
    }

    @Override
    public void updateById(@Nullable final String userId, @Nullable final String id, @Nullable final String name,
                           @NotNull final String description) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @Nullable final E entity;
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final IOwnerRepository<E> repository = getRepository(connection);
            entity = repository.findById(userId, id);
            if (entity == null) throw new EntityNotFoundException();
            entity.setName(name);
            entity.setDescription(description);
        } catch (SQLException exception) {
            throw new DatabaseException();
        }
    }

    @Override
    public void updateByIndex(@Nullable final String userId, @Nullable final Integer index, @Nullable final String name,
                              @NotNull final String description) {
        if (index == null) throw new EmptyIndexException();
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index < 0 || index > getSize(userId)) throw new IndexIncorrectException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @Nullable final E entity;
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final IOwnerRepository<E> repository = getRepository(connection);
            entity = repository.findByIndex(index);
            if (entity == null) throw new EntityNotFoundException();
            entity.setName(name);
            entity.setDescription(description);
        } catch (SQLException exception) {
            throw new DatabaseException();
        }
    }

    @Override
    public boolean existsByName(@Nullable final String userId, @NotNull final String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final IOwnerRepository<E> repository = getRepository(connection);
            return repository.existsByName(userId, name);
        } catch (SQLException exception) {
            throw new DatabaseException();
        }
    }

    @NotNull
    @Override
    public E startById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @Nullable final E entity;
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final IOwnerRepository<E> repository = getRepository(connection);
            entity = repository.startById(userId, id);
        } catch (SQLException exception) {
            throw new DatabaseException();
        }
        if (entity == null) throw new EntityNotFoundException();
        return entity;
    }

    @NotNull
    @Override
    public E startByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null) throw new EmptyIndexException();
        if (index < 0 || index > getSize(userId)) throw new IndexIncorrectException();
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final IOwnerRepository<E> repository = getRepository(connection);
            return repository.startByIndex(userId, index);
        } catch (SQLException exception) {
            throw new DatabaseException();
        }
    }

    @NotNull
    @Override
    public E startByName(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final IOwnerRepository<E> repository = getRepository(connection);
            return repository.startByName(userId, name);
        } catch (SQLException exception) {
            throw new DatabaseException();
        }
    }

    @NotNull
    @Override
    public E finishById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @Nullable final E entity;
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final IOwnerRepository<E> repository = getRepository(connection);
            entity = repository.finishById(userId, id);
        } catch (SQLException exception) {
            throw new DatabaseException();
        }
        if (entity == null) throw new EntityNotFoundException();
        return entity;
    }

    @NotNull
    @Override
    public E finishByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null) throw new EmptyIndexException();
        if (index < 0 || index > getSize(userId)) throw new IndexIncorrectException();
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final IOwnerRepository<E> repository = getRepository(connection);
            return repository.finishByIndex(userId, index);
        } catch (SQLException exception) {
            throw new DatabaseException();
        }
    }

    @NotNull
    @Override
    public E finishByName(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final IOwnerRepository<E> repository = getRepository(connection);
            return repository.finishByName(userId, name);
        } catch (SQLException exception) {
            throw new DatabaseException();
        }
    }

    @NotNull
    @Override
    public E changeStatusById(@Nullable final String userId, @Nullable final String id, @Nullable final Status status) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (status == null) throw new EmptyStatusException();
        @Nullable final E entity;
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final IOwnerRepository<E> repository = getRepository(connection);
            entity = repository.changeStatusById(userId, id, status);
        } catch (SQLException exception) {
            throw new DatabaseException();
        }
        if (entity == null) throw new EntityNotFoundException();
        return entity;
    }

    @NotNull
    @Override
    public E changeStatusByIndex(@Nullable final String userId, @Nullable final Integer index,
                                 @Nullable final Status status) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null) throw new EmptyIndexException();
        if (index < 0 || index > getSize(userId)) throw new IndexIncorrectException();
        if (status == null) throw new EmptyStatusException();
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final IOwnerRepository<E> repository = getRepository(connection);
            return repository.changeStatusByIndex(userId, index, status);
        } catch (SQLException exception) {
            throw new DatabaseException();
        }
    }

    @NotNull
    @Override
    public E changeStatusByName(@Nullable final String userId, @Nullable final String name,
                                @Nullable final Status status) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (status == null) throw new EmptyStatusException();
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final IOwnerRepository<E> repository = getRepository(connection);
            return repository.changeStatusByName(userId, name, status);
        } catch (SQLException exception) {
            throw new DatabaseException();
        }
    }

}
