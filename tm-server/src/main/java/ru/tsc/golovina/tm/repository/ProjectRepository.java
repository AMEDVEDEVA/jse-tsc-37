package ru.tsc.golovina.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.tsc.golovina.tm.api.repository.IProjectRepository;
import ru.tsc.golovina.tm.enumerated.Status;
import ru.tsc.golovina.tm.exception.AbstractException;
import ru.tsc.golovina.tm.model.Project;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

public final class ProjectRepository extends AbstractOwnerRepository<Project> implements IProjectRepository {

    public ProjectRepository(@NotNull Connection connection) {
        super(connection);
    }

    @NotNull
    @Override
    public Project fetch(final @NotNull ResultSet row) throws AbstractException, SQLException {
        @NotNull final Project project = new Project();
        project.setId(row.getString("id"));
        project.setUserId(row.getString("user_id"));
        project.setName(row.getString("name"));
        project.setDescription(row.getString("description"));
        project.setStatus(Status.valueOf(row.getString("status")));
        project.setCreateDate(row.getTimestamp("create_date"));
        project.setStartDate(row.getTimestamp("start_date"));
        return project;
    }

    @NotNull
    @Override
    public String getTableName() {
        return "project";
    }

}
