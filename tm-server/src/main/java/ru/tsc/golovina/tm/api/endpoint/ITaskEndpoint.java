package ru.tsc.golovina.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.golovina.tm.enumerated.Status;
import ru.tsc.golovina.tm.model.Session;
import ru.tsc.golovina.tm.model.Task;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import java.util.List;

public interface ITaskEndpoint {

    @WebMethod
    void add(
            @Nullable @WebParam(name = "session") Session session,
            @NotNull @WebParam(name = "entity") Task entity
    );

    @WebMethod
    void addAll(
            @Nullable @WebParam(name = "session") Session session,
            @NotNull @WebParam(name = "entities") List<Task> entities
    );

    @WebMethod
    void remove(@Nullable @WebParam(name = "session") Session session,
                @NotNull @WebParam(name = "entity") Task entity
    );

    @WebMethod
    @NotNull List<Task> findAll(@Nullable @WebParam(name = "session") Session session);

    @WebMethod
    boolean existsById(
            @Nullable @WebParam(name = "session") Session session,
            @NotNull @WebParam(name = "id") String id
    );

    @WebMethod
    boolean existsByIndex(
            @Nullable @WebParam(name = "session") Session session,
            @NotNull @WebParam(name = "index") Integer index
    );

    @WebMethod
    void clear(@Nullable @WebParam(name = "session") Session session);

    @WebMethod
    @NotNull Task findById(
            @Nullable @WebParam(name = "session") Session session,
            @NotNull @WebParam(name = "id") String id
    );

    @WebMethod
    @Nullable Task findByIndex(
            @Nullable @WebParam(name = "session") Session session,
            @NotNull @WebParam(name = "index") Integer index
    );

    @WebMethod
    @Nullable Task removeById(
            @Nullable @WebParam(name = "session") Session session,
            @NotNull @WebParam(name = "id") String id
    );

    @WebMethod
    @Nullable Task removeByIndex(
            @Nullable @WebParam(name = "session") Session session,
            @NotNull @WebParam(name = "index") Integer index
    );

    @WebMethod
    @NotNull List<Task> findAllByUserId(@Nullable @WebParam(name = "session") Session session);

    @WebMethod
    @NotNull Task findByIdUSerId(
            @Nullable @WebParam(name = "session") Session session,
            @Nullable @WebParam(name = "id") String id
    );

    @WebMethod
    @NotNull Task findByIndexUserId(
            @Nullable @WebParam(name = "session") Session session,
            @Nullable @WebParam(name = "index") Integer index
    );

    @WebMethod
    void clearByUserId(@Nullable @WebParam(name = "session") Session session);

    @WebMethod
    void removeByUserId(
            @Nullable @WebParam(name = "session") Session session,
            @Nullable @WebParam(name = "entity") Task entity
    );

    @WebMethod
    @NotNull Integer getSize(@Nullable @WebParam(name = "session") Session session);

    @WebMethod
    void create(
            @Nullable @WebParam(name = "session") Session session,
            @Nullable @WebParam(name = "name") String name
    );

    @WebMethod
    void create(
            @Nullable @WebParam(name = "session") Session session,
            @Nullable @WebParam(name = "name") String name,
            @Nullable @WebParam(name = "description") String description
    );

    @WebMethod
    @NotNull Task removeByName(
            @Nullable @WebParam(name = "session") Session session,
            @Nullable @WebParam(name = "name") String name
    );

    @WebMethod
    void updateById(
            @Nullable @WebParam(name = "session") Session session,
            @Nullable @WebParam(name = "id") String id,
            @Nullable @WebParam(name = "name") String name,
            @Nullable @WebParam(name = "description") String description
    );

    @WebMethod
    void updateByIndex(
            @Nullable @WebParam(name = "session") Session session,
            @Nullable @WebParam(name = "index") Integer index,
            @Nullable @WebParam(name = "name") String name,
            @Nullable @WebParam(name = "description") String description
    );

    @WebMethod
    boolean existsByName(
            @Nullable @WebParam(name = "session") Session session,
            @NotNull @WebParam(name = "name") String name
    );

    @WebMethod
    @NotNull Task findByName(
            @Nullable @WebParam(name = "session") Session session,
            @Nullable @WebParam(name = "name") String name
    );

    @WebMethod
    @NotNull Task startById(
            @Nullable @WebParam(name = "session") Session session,
            @Nullable @WebParam(name = "id") String id
    );

    @WebMethod
    @NotNull Task startByIndex(
            @Nullable @WebParam(name = "session") Session session,
            @Nullable @WebParam(name = "index") Integer index
    );

    @WebMethod
    @NotNull Task startByName(
            @Nullable @WebParam(name = "session") Session session,
            @Nullable @WebParam(name = "name") String name
    );

    @WebMethod
    @NotNull Task finishById(
            @Nullable @WebParam(name = "session") Session session,
            @Nullable @WebParam(name = "id") String id
    );

    @WebMethod
    @NotNull Task finishByIndex(
            @Nullable @WebParam(name = "session") Session session,
            @Nullable @WebParam(name = "index") Integer index
    );

    @WebMethod
    @NotNull Task finishByName(
            @Nullable @WebParam(name = "session") Session session,
            @Nullable @WebParam(name = "name") String name
    );

    @WebMethod
    @NotNull Task changeStatusById(
            @Nullable @WebParam(name = "session") Session session,
            @Nullable @WebParam(name = "id") String id,
            @Nullable @WebParam(name = "status") Status status
    );

    @WebMethod
    @NotNull Task changeStatusByIndex(
            @Nullable @WebParam(name = "session") Session session,
            @Nullable @WebParam(name = "index") Integer index,
            @Nullable @WebParam(name = "status") Status status
    );

    @WebMethod
    @NotNull Task changeStatusByName(
            @Nullable @WebParam(name = "session") Session session,
            @Nullable @WebParam(name = "name") String name,
            @Nullable @WebParam(name = "status") Status status
    );

}
