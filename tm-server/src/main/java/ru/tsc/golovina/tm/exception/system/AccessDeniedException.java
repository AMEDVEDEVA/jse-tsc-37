package ru.tsc.golovina.tm.exception.system;

import ru.tsc.golovina.tm.exception.AbstractException;

public class AccessDeniedException extends AbstractException {

    public AccessDeniedException() {
        super("Error. Access denied.");
    }

}
