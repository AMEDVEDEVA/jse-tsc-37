package ru.tsc.golovina.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.golovina.tm.api.repository.IUserRepository;
import ru.tsc.golovina.tm.enumerated.Role;
import ru.tsc.golovina.tm.exception.AbstractException;
import ru.tsc.golovina.tm.model.User;
import ru.tsc.golovina.tm.util.HashUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.function.Predicate;

public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    public UserRepository(@NotNull Connection connection) {
        super(connection);
    }

    @Override
    protected @NotNull User fetch(@NotNull ResultSet row) throws AbstractException, SQLException {
        @NotNull final User user = new User();
        user.setEmail(row.getString("email"));
        user.setLogin(row.getString("login"));
        user.setId(row.getString("id"));
        user.setRole(Role.valueOf(row.getString("role")));
        user.setLocked(row.getBoolean("locked"));
        user.setFirstName(row.getString("first_name"));
        user.setLastName(row.getString("last_name"));
        user.setMiddleName(row.getString("middle_name"));
        user.setPassword(HashUtil.salt(row.getString("password_hash")));
        return user;
    }

    protected String getTableName() {
        return "user";
    }

    @Nullable
    @Override
    public User findUserByLogin(@Nullable final String login) throws SQLException {
        @NotNull final String query = "SELECT * FROM " + getTableName() + " WHERE login = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, login);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        final boolean hasNext = resultSet.next();
        if (!hasNext) return null;
        @NotNull final User result = fetch(resultSet);
        statement.close();
        return result;
    }

    @Nullable
    @Override
    public User findUserByEmail(@NotNull final String email) throws SQLException {
        @NotNull final String query = "SELECT * FROM " + getTableName() + " WHERE email = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, email);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        final boolean hasNext = resultSet.next();
        if (!hasNext) return null;
        @NotNull final User result = fetch(resultSet);
        statement.close();
        return result;
    }

    @NotNull
    @Override
    public User removeUserById(@NotNull final String id) throws SQLException {
        @NotNull final User user = findById(id);
        list.remove(user);
        return user;
    }

    @Nullable
    @Override
    public User removeUserByLogin(@NotNull final String login) throws SQLException {
        @Nullable final User user = findUserByLogin(login);
        if (user == null) return null;
        list.remove(user);
        return user;
    }

    @Override
    public boolean userExistsByLogin(@NotNull final String login) throws SQLException {
        return findUserByLogin(login) != null;
    }

    @Override
    public boolean userExistsByEmail(@NotNull final String email) throws SQLException {
        return findUserByEmail(email) != null;
    }

}
